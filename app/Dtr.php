<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dtr extends Model
{
    protected $table = 'dtr';

    protected $fillable =['subscription_id','user','date','punch'];

    public function employee(){
        return $this->hasOne('App\User','id','user');
    }

    public function punch_type(){
        return $this->hasOne('App\PunchType','id','punch');
    }
}

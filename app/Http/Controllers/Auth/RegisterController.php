<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Subscription;
use App\SubscriptionPlan;
use App\SubscriptionPayment;
use DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\ExecutePayment;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payee;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Redirect;
use Session;
use URL;
use DateTime;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    private $_api_context;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');

        //$date = new DateTime('+1 months');
        //dd(date_format($date,'Y-m-d'));
      
        $this->subscription_plans = SubscriptionPlan::pluck('name','id');

        $paypal_conf = \Config::get('paypal');
      
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data,$user)
    {
        
       return  User::create([
            'subscription_id' => $user,
            'name' => $data['owner_name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'user_type' => 2,
        ]);
    }

    public function showRegistrationForm()
    {
        return view('auth.register')
                ->with('subscription_plans',$this->subscription_plans);
    }

    public function register($request)
    {
                     
        DB::beginTransaction();

        $subs = $this->createSubcriptionInfo($request);
        
        if($request['plan'] != 1){
            
            $payment = $this->storePayment($subs->id);
        
        }
       
        if($subs){
            
            $user = $this->create($request,$subs->id);

           if($user){
               
                DB::commit();

                $this->guard()->login($user);          
            
               // $this->registered($request, $user);
                return $this->registered($request, $user)
                                ?: redirect('/register');
        
           }else{
                DB::rollback();
                return redirect('/register');
                
           }

        }else{
            
            DB::rollback();
            return redirect('/register');
        }
        
                     
    }

    protected function guard()
    {
        return Auth::guard();
    }

    protected function registered($request, $user)
    {   
        $plan = SubscriptionPlan::find($request['plan']);    

        if($request['plan'] != 1){
            Session::put('register_success','Welcome To PunchTime! Thank you for subscribing on us, '.$plan->description);
        }else{
            Session::put('register_success','Welcome To PunchTime! Thank you for subscribing on us, '.$plan->description);   
        }
        return redirect('/home');
    }

    public function storePayment($subs)
    {
        $payment = SubscriptionPayment::create([
                            'subscription_id' => $subs,
                            'amount_paid' => Session::get('pay_amount'),
                            'payment_status' => Session::get('payment_status'),
                            ]);
        return $payment;                    
    }

    protected function createSubcriptionInfo($request)
    {
        
        $plan = SubscriptionPlan::find($request['plan']);
        $expiry_date = new DateTime($plan->code);

                //dd(date_format($date,'Y-m-d'));
        $subscription = Subscription::create([
                        'plan_id'      => $request['plan'], 
                        'plan_expiry_date' => $expiry_date,
                        'company_name' => $request['company_name'],
                        'owner_name'   => $request['owner_name'],
                        'email'        => $request['email'],
                        'contact_no'   => $request['contact_no'],
                        'status'       => 1,
                        ]);

       
        return $subscription;                
    }

    public function pay(Request $request)
    {       
        Session::put('subscription_data',$request->all()); 
        if($request->plan != 1){
          
            $plan = SubscriptionPlan::find($request->plan);       

            $data_token = $request->_token;
            $toPay = $plan->price;

            Session::put('pay_amount', $plan->price);
            Session::put('data_token', $data_token);
            

            $payer = new Payer();

            $payer->setPaymentMethod('paypal');
            
            $item_1 = new Item();
            $item_1->setName('Punchtime '.$plan->name.' subscription payment')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setPrice($toPay);
            
            $item_list = new ItemList();
            $item_list->setItems(array($item_1));
            /* $details = new Details();
            $details->setShipping($tax)
                ->setTax($tax)
                ->setSubtotal($amount); */
            
            $amount = new Amount();
            $amount->setCurrency('USD')
                ->setTotal($toPay);
                // ->setDetails($details);
            /* $payee = new Payee();
            $payee->setEmail($user_paypal); */
            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($item_list)
                // ->setPayee($payee)
                ->setDescription('PunchTime Subscription')
                ->setInvoiceNumber(uniqid());
            $redirect_urls = new RedirectUrls();
            $redirect_urls->setReturnUrl(URL::to('/subscription-payment-status?success=true'))
                ->setCancelUrl(URL::to('login'));
            
            $payment = new Payment();
            $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));
            
            try{
                $payment->create($this->_api_context);
                
            }catch(\Paypal\Exception\PPConnectionException $e){
                if(\Config::get('app.debug')){
                    \Session::put('error', 'Connection Timeout');
                    // return Redirect::route('projects');
                    return back();
                }else{
                    \Session::put('error', 'An error occured');
                    // return Redirect::route('projects');
                    return back();
                }
            }
            foreach($payment->getLinks() as $link){
                if($link->getRel() == 'approval_url'){
                    $redirect_url = $link->getHref();
                    break;
                }
            }
            
            Session::put('paypal_payment_id', $payment->getId());
            if(isset($redirect_url)){
                return Redirect::away($redirect_url);
            }
            
            \Session::put('error',' Unknown error occured');
            // return Redirect::route('projects');
            return back();
        }else{
           
            $this->register(Session::get('subscription_data'));
        }
    }

        public function paymentStatus(Request $request) {
            
            $paypal_payment_id = $request->get('paymentId');
    
            $total = Session::get('pay_amount');
            $my_token = Session::get('data_token');
    
            Session::forget('paypal_payment_id');
    
            if(empty($request->get('PayerID')) || empty($request->get('token'))){
                \Session::put('error', 'Payment failed');
                // return Redirect::route('projects');
                return back();
            }
            $payment = Payment::get($paypal_payment_id, $this->_api_context);
    
            $execution = new PaymentExecution();
            
            $execution->setPayerId($request->get('PayerID'));
    
            try{
            $result = $payment->execute($execution, $this->_api_context);
            }catch(\Paypal\Exception\PPConnectionException $e){
                if(\Config::get('app.debug')){
                    session()->flash('error_message', 'Sorry but it seems there is a problem processing your payment!');
                    // return Redirect::route('projects');
                    return redirect('/register');
                }else{
                    
                    session()->flash('error_message', 'Sorry but it seems there is a problem processing your payment!');
                    // return Redirect::route('projects');
                    return redirect('/register');
                }
            }
    
            if($result->getState() == 'approved'){               
                    Session::put('payment_status',$result->getState());
                   $this->register(Session::get('subscription_data'));
            }
    
            \Session::put('error','Payment failed');
          
            // return Redirect::route('projects');
            return back();
        }
        
    
}

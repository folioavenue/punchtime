<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Department;
class DepartmentController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Department',
        'breadcrumb' => 'Department']);
        $this->middleware('auth');
       
        $this->middleware(function ($request,$next){
            $this->departments = Department::where('subscription_id',Auth::user()->getsubscription->id)->get();
           
            return $next($request);
         });      

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('module.department.index')
                ->with('departments',$this->departments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = Department::create([
                        'subscription_id' => Auth::user()->getsubscription->id,
                        'name' => $request->department,
                     ]);

        if($department){
            session()->flash('message','New department is successfully added!');            
        }else{        
            
            session()->flash('error_message','Fail to add new  department!');             
        }             

        return redirect('department');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);

        return view('module.department.index')
                ->with('department',$department)
                ->with('departments',$this->departments);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $department = Department::find($id);

        if($department->name == $request->department){
            session()->flash('warning_message','No changes has been made to selected department!');   
            
            return redirect()->back();
        }

        $department->name = $request->department;

        if($department->save()){
            session()->flash('message','Department is successfully updated!');            
        }else{        
            
            session()->flash('error_message','Fail to update department!');             
        }        

        return redirect('department');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

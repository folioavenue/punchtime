<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Department;
use App\Position;
use App\Status;
use App\Shift;
use App\User;
use App\UserStatus;
use App\CivilStatus;
use App\Gender;
use App\Dtr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

class EmployeeController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Employee',
        'breadcrumb' => 'Employee']);
        $this->middleware('auth');
        
        $this->middleware(function ($request,$next){
            $this->departments = Department::where('subscription_id',Auth::user()->getsubscription->id)->pluck('name','id');
            $this->positions = Position::where('subscription_id',Auth::user()->getsubscription->id)->pluck('name','id');
            $this->statuses = Status::where('subscription_id',Auth::user()->getsubscription->id)->pluck('name','id');
            $this->shifts = Shift::where('subscription_id',Auth::user()->getsubscription->id)->pluck('name','id');
            $this->employees = User::where('subscription_id',Auth::user()->getsubscription->id)->where('user_type',3)->get();
            $this->user_statuses = UserStatus::pluck('name','id');
            $this->civil_statuses = CivilStatus::pluck('name','id');      
            $this->gender = Gender::pluck('name','id');      
            
            

            return $next($request);
        });      
  
    }
    /**)
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        view()->share(['breadcrumb' => 'Employee Lists']);
        return view('module.employee.index')
                ->with('employees',$this->employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        view()->share(['breadcrumb' => 'Add New Employee']);
        
        return view('module.employee.create')
                    ->with('departments',$this->departments)
                    ->with('positions',$this->positions)
                    ->with('statuses',$this->statuses)
                    ->with('shifts',$this->shifts)
                    ->with('user_statuses',$this->user_statuses)
                    ->with('civil_statuses',$this->civil_statuses)
                    ->with('gender',$this->gender);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $file = $request->file('avatar');

        if($file != null){

            $img = $file->store('avatar');
        
        }else{

            $img = null;   

        }

        
        if($this->checkIfEmailExist($request->email))
        {
            session()->flash('error_message','Email is already Exist!');            
            return redirect()->back();
        }

        if($this->checkIfUsernameExist($request->username))
        {
            session()->flash('error_message','Username is already used!');            
            return redirect()->back();
        }

        if($request->password == $request->cpassword){
            $user = User::create([
                'subscription_id' => Auth::user()->getsubscription->id,
                'name'  => $request->name,
                'email' => $request->email,
                'birthdate' => $request->birthday,
                'civil_status' => $request->civil_status,
                'gender'   => $request->gender,
                'department' => $request->department,
                'position'  => $request->position,
                'status'    => $request->status,
                'shift'     => $request->shift,
                'address'   => $request->address,
                'contact_no'=> $request->contact_no,
                'date_hired'=> date('y-m-d', strtotime($request->datehired)),
                'sl_credits'=> $request->sl_credits,
                'vl_credits'=> $request->vl_credits,
                'avatar'    => $img,
                'username'  => $request->username,
                'password'  => Hash::make($request->password),
                'user_status' => $request->account_status,
                'user_type' => 3,
                ]);
        }else{
            session()->flash('error_message','Password and Confirm Password did not match!');            
            return redirect()->back();
        }

        session()->flash('message','New employee data successfully added!');
        
        return redirect('employee\create');
    }

    public function checkIfEmailExist($email)
    {
        $exist = false;

        $users = User::get();
        
        foreach($users as $user){
            
            if($user->email == $email)
            {
                $exist = true;
                break;    
            }

        }

        return $exist;
        
    }

    public function checkIfUsernameExist($username)
    {
        $exist = false;

        $users = User::get();
        
        foreach($users as $user){
            
            if($user->username == $username)
            {
                $exist = true;
                break;    
            }

        }

        return $exist;
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        view()->share(['breadcrumb' => 'Employee Profile']);

        $employee = User::find($id);

        return view('module.employee.show')
                ->with('employee',$employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        view()->share(['breadcrumb' => 'Update Employee']);
        $employee = User::find($id);

        return view('module.employee.create')
                ->with('employee',$employee)
                ->with('departments',$this->departments)
                ->with('positions',$this->positions)
                ->with('statuses',$this->statuses)
                ->with('shifts',$this->shifts)
                ->with('user_statuses',$this->user_statuses)
                ->with('civil_statuses',$this->civil_statuses)
                ->with('gender',$this->gender);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $employee = User::find($id);

       $employee->name = $request->name;
       $employee->birthdate = Carbon::parse($request->birthdate)->format('Y-m-d');
       $employee->civil_status = $request->civil_status;
       $employee->gender = $request->gender;
       $employee->email = $request->email;
       $employee->department = $request->department;
       $employee->position = $request->position;
       $employee->status = $request->status;
       $employee->shift = $request->shift;
       $employee->contact_no = $request->contact_no;
       $employee->sl_credits = $request->sl_credits;
       $employee->vl_credits = $request->vl_credits;
       
       if($request->file('avatar') != null){
        $file = $request->file('avatar');   
        $avatar = $file->store('avatar');
        $employee->avatar = $avatar;
       }

       if($request->password != null){
            if($request->password == $request->cpassword){
               
                $employee->password = Hash::make($request->password);
                
            }else{
                session()->flash('error_message','Password and Confirm Password did not match!');            
                return redirect()->back();
            }
        }
        
        if($employee->save()){
            session()->flash('message','Employee data successfully updated!');
            return redirect('employee');
        }else{
            session()->flash('error_message','Fail to update employee data!');            
            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Leave;
use App\Overtime;
use App\Undertime;
use App\Dtr;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        view()->share(['page_title' => 'Dashboard',
        'breadcrumb' => 'Dashboard']);
        $this->middleware('auth');

        $this->middleware(function ($request,$next){

            
           $this->leave = Leave::where('subscription_id',Auth::user()->subscription_id)->orderBy('created_at','desc')->get();  
           $this->overtime = Overtime::where('subscription_id',Auth::user()->subscription_id)->orderBy('created_at','desc')->get();  
           $this->undertime = Undertime::where('subscription_id',Auth::user()->subscription_id)->orderBy('created_at','desc')->get();  
           $this->dtr = Dtr::where('subscription_id',Auth::user()->subscription_id)->whereDate('date',Carbon::parse(date('Y-m-d')))->orderBy('created_at','desc')->get();  
           $this->check_dtr = Dtr::where('user',Auth::user()->id)->whereDate('date',date('Y-m-d'))->orderBy('date','desc')->first();
           
            
           
            return $next($request);
         });  
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home')
                ->with('leave',$this->leave)
                ->with('overtime',$this->overtime)
                ->with('undertime',$this->undertime)
                ->with('dtr',$this->dtr)
                ->with('check_dtr',$this->check_dtr);
    }
}

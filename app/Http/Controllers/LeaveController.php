<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Leave;
use App\LeaveCreditType;
use App\RequestStatus;
use App\LeaveType;

class LeaveController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Leave Request',
        'breadcrumb' => 'Leave Request']);
        $this->middleware('auth');
       
        $this->middleware(function ($request,$next){

            if(Auth::user()->user_type == 3 ){
                 $this->leave = Leave::where('employee',Auth::user()->id)->orderBy('created_at','desc')->get();
            }else{
                $this->leave = Leave::where('subscription_id',Auth::user()->subscription_id)->orderBy('created_at','desc')->get();  
            }

            $this->leave_types = LeaveType::pluck('name','id');
            $this->leave_credit_types = LeavecreditType::pluck('name','id');
           
            return $next($request);
         });      

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('module.leave.index')
                ->with('leave',$this->leave)
                ->with('leave_types',$this->leave_types)
                ->with('leave_credit_types',$this->leave_credit_types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = explode('-',$request->effective_date);
        
        $from_date = New \DateTime($date[0].' 0:00:00');
        $to_date = New \DateTime($date[1].' 24:00:00');
        //dd($to_date);
        $diff_in_days = $from_date->diff($to_date);

        
        if($this->checkCredits(Auth::user()->id,$request->leave_type,(int) $diff_in_days->format('%a'),$request->leave_credit_type) || ($request->leave_type == 1 || $request->leave_type == 2) ){

            $leave = Leave::create([
                            'subscription_id' => Auth::user()->getsubscription->id,
                            'employee' => Auth::user()->id,
                            'from_date' => Carbon::parse($date[0])->format('Y-m-d'),
                            'to_date'   => Carbon::parse($date[1])->format('Y-m-d'),
                            'reason'    => $request->reason,
                            'leave_type' => $request->leave_type,
                            'credit_type' => $request->leave_credit_type,
                            'status'    => 4,
                            ]);

            if($leave){
                session()->flash('message','Leave request successfully send!');            
            }else{        
                
                session()->flash('error_message','Fail to send new leave request!');             
            }
        }else{
            if($request->leave_type == 3){
                
                session()->flash('error_message','Fail to file request, Your did not have enough sick leave credits!');   
            }elseif($request->leave_type == 4){
                session()->flash('error_message','Fail to file request, Your did not have enough vacation leave credits!');   
                
            }
        }             

        return redirect('leave');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cancelRequest(Request $request)
    {
        $bol = "";

        $leave = Leave::find($request->id);        

        $leave->status = 3;

        if($leave->save()){
            
            if(($leave->leave_type == 3 || $leave->leave_type == 4) && $leave->credit_type = 1){
                
                $employee = User::find($leave->employee);
                
                if($leave->leave_type == 3){
                    $employee->sl_credits = $employee->sl_credits + $request->no_of_days;
                }else{
                    $employee->vl_credits = $employee->vl_credits + $request->no_of_days;
                }               
                
                $employee->save();
            }

            $bol = "Leave request successfully cancelled!";
        }else{
            $bol = "Fail to cancel request.<br/>Pls. contact administrator!";
        }

        return $bol;
    }

    public function updateRequest(Request $request)
    {
        $bol = "";
        $word_status_success = "";
        $word_status_fail = "";

        $leave = Leave::find($request->id);     

       
        
        if($request->status == 1){
    
            $word_status_success = "approved";
            $word_status_fail = "approve";
            $leave->approved_by = Auth::user()->id;

        }else{

            if(($leave->leave_type == 3 || $leave->leave_type == 4) && $leave->credit_type = 1){
                
                $employee = User::find($leave->employee);
                
                if($leave->leave_type == 3){
                    $employee->sl_credits = $employee->sl_credits + $request->no_of_days;
                }else{
                    $employee->vl_credits = $employee->vl_credits + $request->no_of_days;
                }               
                $employee->save();
            }

            $word_status_success = "disapproved";
            $word_status_fail = "disapprove";
        }

        
        $leave->status = $request->status;
        

        if($leave->save()){
            $bol = "Leave request successfully ".$word_status_success."!";
        }else{
            $bol = "Fail to ".$word_status_fail." leave request.<br/>Pls. contact administrator!";
        }

        return $bol;
    }

    public function checkCredits($id,$leave_type,$no_of_days,$credit_type)
    {
        
        $employee = User::find($id);

        if($credit_type == 1){
               
                if($leave_type == 3){
                    if($employee->sl_credits != 0 && $no_of_days <= $employee->sl_credits ){
                        
                        $employee->sl_credits = $employee->sl_credits - $no_of_days;      
                    }else{
                        return false;
                    }
                }elseif($leave_type == 4){
                    if($employee->vl_credits != 0 && $no_of_days <= $employee->vl_credits){
                        
                        $employee->vl_credits = $employee->vl_credits - $no_of_days;      
                    }else{
                        return false;
                    }
                }

                
                if($employee->save())
                {
                    return true;
                }else{
                    return false;
                }
            
        }else{
            return true;
        }
    }
}

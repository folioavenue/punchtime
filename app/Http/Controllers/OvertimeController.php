<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Overtime;


class OvertimeController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Overtime Request',
        'breadcrumb' => 'Overtime Request']);
        $this->middleware('auth');
       
        $this->middleware(function ($request,$next){

            if(Auth::user()->user_type == 3 ){
                 $this->overtime = Overtime::where('employee',Auth::user()->id)->orderBy('created_at','desc')->get();
            }else{
                $this->overtime = Overtime::where('subscription_id',Auth::user()->subscription_id)->orderBy('created_at','desc')->get();  
            }

            
           
            return $next($request);
         });      

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('module.overtime.index')
                ->with('overtime',$this->overtime);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $overtime = Overtime::create([
                        'subscription_id' => Auth::user()->getsubscription->id,
                        'employee' => Auth::user()->id,
                        'no_of_hours' => $request->no_of_hours,
                        'date' => Carbon::parse($request->date)->format('Y-m-d'),                        
                        'reason'    => $request->reason,
                        'status'    => 4,
                        ]);

        if($overtime){
            session()->flash('message','Overtime request successfully send!');            
        }else{        
            
            session()->flash('error_message','Fail to send new overtime request!');             
        }             

        return redirect('overtime');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cancelRequest(Request $request)
    {
        $bol = "";

        $overtime = Overtime::find($request->id);

        $overtime->status = 3;
        
        if($overtime->save()){
            $bol = "Overtime request successfully cancelled!";
        }else{
            $bol = "Fail to cancel overtime request.<br/>Pls. contact administrator!";
        }

        return $bol;
    }

    public function updateRequest(Request $request)
    {
        $bol = "";
        $word_status_success = "";
        $word_status_fail = "";

        if($request->status == 1){
            $word_status_success = "approved";
            $word_status_fail = "approve";
        }else{
            $word_status_success = "disapproved";
            $word_status_fail = "disapprove";
        }

        $overtime = Overtime::find($request->id);

        $overtime->status = $request->status;
        $overtime->approved_by = Auth::user()->id;

        if($overtime->save()){
            $bol = "Overtime request successfully ".$word_status_success."!";
        }else{
            $bol = "Fail to ".$word_status_fail." overtime request.<br/>Pls. contact administrator!";
        }

        return $bol;
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Position;

class PositionController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Position',
        'breadcrumb' => 'Position']);
        $this->middleware('auth');

        $this->middleware(function ($request,$next){
            $this->positions = Position::where('subscription_id',Auth::user()->getsubscription->id)->get();

            return $next($request);
        }); 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('module.position.index')
                ->with('positions',$this->positions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $position = Position::create([
                        'subscription_id' => Auth::user()->getsubscription->id,
                        'name' => $request->position,
                     ]);

        if($position){
            session()->flash('message','New position is successfully added!');            
        }else{        
            
            session()->flash('error_message','Fail to add new  position!');             
        }             

        return redirect('position');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = Position::find($id);

        return view('module.position.index')
                ->with('position',$position)
                ->with('positions',$this->positions);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $position = Position::find($id);

        if($position->name == $request->position){
            session()->flash('warning_message','No changes has been made to selected position!');   
            
            return redirect()->back();
        }

        $position->name = $request->position;

        if($position->save()){
            session()->flash('message','New position is successfully updated!');            
        }else{        
            
            session()->flash('error_message','Fail to update position!');             
        }        

        return redirect('position');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Shift;
use App\ShiftDay;
use App\ShiftBreak;
use Carbon\Carbon;
use DB;
class ShiftController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Shift Schedule',
        'breadcrumb' => 'Shift Schedule']);
        $this->middleware('auth');
       
        $this->middleware(function ($request,$next){
            $this->shifts = Shift::where('subscription_id',Auth::user()->getsubscription->id)->get();
            
           
            return $next($request);
         });      

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('module.shift.index')
                ->with('shifts',$this->shifts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('module.shift.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $monday_from = $request->monday_from != null ? Carbon::parse($request->monday_from)->format('H:i:s') : null;
        $monday_to = $request->monday_to != null ? Carbon::parse($request->monday_to)->format('H:i:s') : null;

        $tuesday_from = $request->tuesday_from != null ? Carbon::parse($request->tuesday_from)->format('H:i:s') : null;
        $tuesday_to = $request->tuesday_to != null ? Carbon::parse($request->tuesday_to)->format('H:i:s') : null;

        $wednesday_from = $request->wednesday_from != null ? Carbon::parse($request->wednesday_from)->format('H:i:s') : null;
        $wednesday_to = $request->wednesday_to != null ? Carbon::parse($request->wednesday_to)->format('H:i:s') : null;

        $thursday_from = $request->thursday_from != null ? Carbon::parse($request->thursday_from)->format('H:i:s') : null;
        $thursday_to = $request->thursday_to != null ? Carbon::parse($request->thursday_to)->format('H:i:s') : null;

        $friday_from = $request->friday_from != null ? Carbon::parse($request->friday_from)->format('H:i:s') : null;
        $friday_to = $request->friday_to != null ? Carbon::parse($request->friday_to)->format('H:i:s') : null;

        $saturday_from = $request->saturday_from != null ? Carbon::parse($request->saturday_from)->format('H:i:s') : null;
        $saturday_to = $request->saturday_to != null ? Carbon::parse($request->saturday_to)->format('H:i:s') : null;

        $sunday_from = $request->sunday_from != null ? Carbon::parse($request->sunday_from)->format('H:i:s') : null;
        $sunday_to = $request->sunday_to != null ? Carbon::parse($request->sunday_to)->format('H:i:s') : null;

        $days = [
                1 => 'monday',
                2 => 'tuesday',
                3 => 'wednesday',
                4 => 'thursday',
                5 => 'friday',
                6 => 'saturday',
                7 => 'sunday',
        ];
           
      // dd($request->all());
        DB::beginTransaction();
        $shift = Shift::create([
                    'subscription_id' => Auth::user()->getsubscription->id,
                    'name' => $request->name,  
                    'grace_time' => $request->grace_time,                  
                    ]);

        if($shift) {

          if($request->option == null){  
            
            foreach($days as $key => $val){               
                
                $advance_shift =  ShiftDay::create(['shift' => $shift->id,
                                                'days' => $val,
                                                'shift_start' => $key >= (int) $request->from_day && $key <= (int) $request->to_day ? Carbon::parse($request->from_time)->format('H:i:s') : null,
                                                'shift_end' => $key >= (int) $request->from_day && $key <= (int) $request->to_day ? Carbon::parse($request->to_time)->format('H:i:s') : null,
                                                ]);
            }   

            if($advance_shift){
                DB::commit();
            }else{
                session()->flash('error_message','Fail to add new  shift schedule!');  
                DB::rollback();
                return redirect()->back();
            }

          }else{  
            $monday_shift =  ShiftDay::create(['shift' => $shift->id,'days' => 'monday','shift_start' => $monday_from,'shift_end' => $monday_to]);
            $tuesday_shift =  ShiftDay::create(['shift' => $shift->id,'days' => 'tuesday','shift_start' => $tuesday_from,'shift_end' => $tuesday_to]);
            $wednesday_shift =  ShiftDay::create(['shift' => $shift->id,'days' => 'wednesday','shift_start' => $wednesday_from,'shift_end' => $wednesday_to]);
            $thursday_shift =  ShiftDay::create(['shift' => $shift->id,'days' => 'thursday','shift_start' => $thursday_from,'shift_end' => $thursday_to]);
            $friday_shift =  ShiftDay::create(['shift' => $shift->id,'days' => 'friday','shift_start' => $friday_from,'shift_end' => $friday_to]);
            $saturday_shift =  ShiftDay::create(['shift' => $shift->id,'days' => 'saturday','shift_start' => $saturday_from,'shift_end' => $saturday_to]);
            $sunday_shift =  ShiftDay::create(['shift' => $shift->id,'days' => 'sunday','shift_start' => $sunday_from,'shift_end' => $sunday_to]);

            if($monday_shift && $tuesday_shift && $wednesday_shift && $thursday_shift && $friday_shift && $saturday_shift && $sunday_shift){
                DB::commit();
            }else{
                session()->flash('error_message','Fail to add new  shift schedule!');  
                DB::rollback();
                return redirect()->back();
            }
        }   
            

        }else{
            session()->flash('error_message','Fail to add new  shift schedule!');  
            DB::rollback();
            return redirect()->back();
        }   
        
        session()->flash('message','New shift schedule is successfully added!');        

        return redirect('shift/'.$shift->id.'/edit');           
                    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   $advance_time = ShiftDay::where('shift',$id)->where('shift_start','<>',null)->where('shift_start','<>',null)->first();
        
        $shift = Shift::find($id);

        $monday = $shift->shiftdays->where('days','monday')->first();
        $tuesday = $shift->shiftdays->where('days','tuesday')->first();
        $wednesday = $shift->shiftdays->where('days','wednesday')->first();
        $thursday = $shift->shiftdays->where('days','thursday')->first();
        $friday = $shift->shiftdays->where('days','friday')->first();
        $saturday = $shift->shiftdays->where('days','saturday')->first();
        $sunday = $shift->shiftdays->where('days','sunday')->first();
        
        
        return view('module.shift.create')
                ->with('shift',$shift)
                ->with('monday',$monday)
                ->with('tuesday',$tuesday)
                ->with('wednesday',$wednesday)
                ->with('thursday',$thursday)
                ->with('friday',$friday)
                ->with('saturday',$saturday)
                ->with('sunday',$sunday)
                ->with('advance_time',$advance_time);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $days = [
            1 => 'monday',
            2 => 'tuesday',
            3 => 'wednesday',
            4 => 'thursday',
            5 => 'friday',
            6 => 'saturday',
            7 => 'sunday',
            ];

        $monday_from = $request->monday_from != null ? Carbon::parse($request->monday_from)->format('H:i:s') : null;
        $monday_to = $request->monday_to != null ? Carbon::parse($request->monday_to)->format('H:i:s') : null;

        $tuesday_from = $request->tuesday_from != null ? Carbon::parse($request->tuesday_from)->format('H:i:s') : null;
        $tuesday_to = $request->tuesday_to != null ? Carbon::parse($request->tuesday_to)->format('H:i:s') : null;

        $wednesday_from = $request->wednesday_from != null ? Carbon::parse($request->wednesday_from)->format('H:i:s') : null;
        $wednesday_to = $request->wednesday_to != null ? Carbon::parse($request->wednesday_to)->format('H:i:s') : null;

        $thursday_from = $request->thursday_from != null ? Carbon::parse($request->thursday_from)->format('H:i:s') : null;
        $thursday_to = $request->thursday_to != null ? Carbon::parse($request->thursday_to)->format('H:i:s') : null;

        $friday_from = $request->friday_from != null ? Carbon::parse($request->friday_from)->format('H:i:s') : null;
        $friday_to = $request->friday_to != null ? Carbon::parse($request->friday_to)->format('H:i:s') : null;

        $saturday_from = $request->saturday_from != null ? Carbon::parse($request->saturday_from)->format('H:i:s') : null;
        $saturday_to = $request->saturday_to != null ? Carbon::parse($request->saturday_to)->format('H:i:s') : null;

        $sunday_from = $request->sunday_from != null ? Carbon::parse($request->sunday_from)->format('H:i:s') : null;
        $sunday_to = $request->sunday_to != null ? Carbon::parse($request->sunday_to)->format('H:i:s') : null;

        DB::beginTransaction();

        $shift = Shift::find($id);

        $shift->name = $request->name;
        $shift->grace_time = $request->grace_time;
            //dd($request->all());
        if($shift->save()){

            if($request->option == null){
                foreach($days as $key => $val){     
                   
                    $advance_shift = $shift->shiftdays->where('days',$val)->first();
                    $advance_shift->shift_start = $key >= (int) $request->from_day && $key <= (int) $request->to_day ? Carbon::parse($request->from_time)->format('H:i:s') : null;
                    $advance_shift->shift_end  = $key >= (int) $request->from_day && $key <= (int) $request->to_day ? Carbon::parse($request->to_time)->format('H:i:s') : null;                
               if($advance_shift->save())
                {
                    DB::commit();
                }
                else{
                    DB::rollback();
                    session()->flash('error_message','Fail to update  shift schedule!');  
                    return redirect()->back();
                }
                }   
                
            }else{
                $monday = $shift->shiftdays->where('days','monday')->first();
                $monday->shift_start = $monday_from;
                $monday->shift_end  = $monday_to;

                $tuesday = $shift->shiftdays->where('days','tuesday')->first();
                $tuesday->shift_start = $tuesday_from;
                $tuesday->shift_end  = $tuesday_to;

                $wednesday = $shift->shiftdays->where('days','wednesday')->first();
                $wednesday->shift_start = $wednesday_from;
                $wednesday->shift_end  = $wednesday_to;

                $thursday = $shift->shiftdays->where('days','thursday')->first();
                $thursday->shift_start = $thursday_from;
                $thursday->shift_end  = $thursday_to;

                $friday = $shift->shiftdays->where('days','friday')->first();
                $friday->shift_start = $friday_from;
                $friday->shift_end  = $friday_to;

                $saturday = $shift->shiftdays->where('days','saturday')->first();
                $saturday->shift_start = $saturday_from;
                $saturday->shift_end  = $saturday_to;

                $sunday = $shift->shiftdays->where('days','sunday')->first();
                $sunday->shift_start = $sunday_from;
                $sunday->shift_end  = $sunday_to;

                if($monday->save() && $tuesday->save() && $wednesday->save() && $thursday->save() && $thursday->save() && $friday->save() && $saturday->save() && $sunday->save())
                {
                    DB::commit();
                }
                else{
                    DB::rollback();
                    session()->flash('error_message','Fail to update  shift schedule!');  
                    return redirect()->back();
                }
            }

            
        }else{
            DB::rollback();
            session()->flash('error_message','Fail to update  shift schedule!');  
            return redirect()->back();
        }

        session()->flash('message','Shift schedule is successfully updated!');        

        return redirect('shift/'.$id.'/edit');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storebreakschedule(Request $request)
    {
        $break_from = $request->break_from != null ? Carbon::parse($request->break_from)->format('H:i:s') : null;
        $break_to = $request->break_to != null ? Carbon::parse($request->break_to)->format('H:i:s') : null;

        $shift_break = ShiftBreak::create([
                        'shift' => $request->shift,
                        'name' => $request->name,
                        'from_time' => $break_from,
                        'to_time' => $break_to,
                        ]);

        if($shift_break){
            session()->flash('message','New break schedule is successfully added!');            
        }else{        
            
            session()->flash('error_message','Fail to add new  break schedule!');             
        }             

        return redirect('shift/'.$request->shift.'/edit');          
    }
}

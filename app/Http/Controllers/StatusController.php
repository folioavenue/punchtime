<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Status;
class StatusController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Status',
        'breadcrumb' => 'Status']);
        $this->middleware('auth');
       
        $this->middleware(function ($request,$next){
            $this->statuses = Status::where('subscription_id',Auth::user()->getsubscription->id)->get();
           
            return $next($request);
         });      

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('module.status.index')
                ->with('statuses',$this->statuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = Status::create([
                        'subscription_id' => Auth::user()->getsubscription->id,
                        'name' => $request->status,
                     ]);

        if($department){
            session()->flash('message','New Status is successfully added!');            
        }else{        
            
            session()->flash('error_message','Fail to add new  Status!');             
        }             

        return redirect('status');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Status::find($id);

        return view('module.status.index')
                ->with('status',$status)
                ->with('statuses',$this->statuses);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = Status::find($id);

        if($status->name == $request->status){
            session()->flash('warning_message','No changes has been made to selected status!');   
            
            return redirect()->back();
        }

        $status->name = $request->status;

        if($status->save()){
            session()->flash('message','Status is successfully updated!');            
        }else{        
            
            session()->flash('error_message','Fail to update status!');             
        }        

        return redirect('status');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Undertime;

class UndertimeController extends Controller
{
    public function __construct()
    {
        view()->share(['page_title' => 'Undertime Request',
        'breadcrumb' => 'Undertime Request']);
        $this->middleware('auth');
       
        $this->middleware(function ($request,$next){

            if(Auth::user()->user_type == 3 ){
                 $this->undertime = Undertime::where('employee',Auth::user()->id)->orderBy('created_at','desc')->get();
            }else{
                $this->undertime = Undertime::where('subscription_id',Auth::user()->subscription_id)->orderBy('created_at','desc')->get();  
            }

            
           
            return $next($request);
         });      

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('module.undertime.index')
                ->with('undertime',$this->undertime);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $undertime = Undertime::create([
                        'subscription_id' => Auth::user()->getsubscription->id,
                        'employee' => Auth::user()->id,
                        'date' => Carbon::parse($request->date)->format('Y-m-d'),                        
                        'reason'    => $request->reason,
                        'status'    => 4,
                        ]);

        if($undertime){
            session()->flash('message','Undertime request successfully send!');            
        }else{        
            
            session()->flash('error_message','Fail to send new undertime request!');             
        }             

        return redirect('undertime');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cancelRequest(Request $request)
    {
        $bol = "";

        $undertime = Undertime::find($request->id);

        $undertime->status = 3;
        
        if($undertime->save()){
            $bol = "Undertime request successfully cancelled!";
        }else{
            $bol = "Fail to cancel undertime request.<br/>Pls. contact administrator!";
        }

        return $bol;
    }

    public function updateRequest(Request $request)
    {
        $bol = "";
        $word_status_success = "";
        $word_status_fail = "";

        if($request->status == 1){
            $word_status_success = "approved";
            $word_status_fail = "approve";
        }else{
            $word_status_success = "disapproved";
            $word_status_fail = "disapprove";
        }

        $undertime = Undertime::find($request->id);

        $undertime->status = $request->status;
        $undertime->approved_by = Auth::user()->id;

        if($undertime->save()){
            $bol = "Undertime request successfully ".$word_status_success."!";
        }else{
            $bol = "Fail to ".$word_status_fail." undertime request.<br/>Pls. contact administrator!";
        }

        return $bol;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $table = 'leave';

    protected $fillable = ['subscription_id','employee','from_date','to_date','reason','leave_type','credit_type','approved_by','status'];

    public function getEmployee()
    {
        return $this->hasOne('App\User','id','employee');
    }

    public function getLeaveType()
    {
        return $this->hasOne('App\LeaveType','id','leave_type');
    }

    public function getCreditType()
    {
        return $this->hasOne('App\LeaveCreditType','id','credit_type');
    }

    public function getRequestStatus()
    {
        return $this->hasOne('App\RequestStatus','id','status');
    }
   
    public function getApproval()
    {
        return $this->hasOne('App\User','id','approved_by');
    }
}

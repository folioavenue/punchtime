<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveCreditType extends Model
{
    protected $table = 'leave_credit_types';

    protected $fillable = ['name'];
}

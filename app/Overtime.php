<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Overtime extends Model
{
    protected $table = 'overtime';

    protected $fillable = ['subscription_id','employee','date','no_of_hours','reason','approved_by','status'];

    public function getEmployee()
    {
        return $this->hasOne('App\User','id','employee');
    }

    public function getRequestStatus()
    {
        return $this->hasOne('App\RequestStatus','id','status');
    }

    public function getApproval()
    {
        return $this->hasOne('App\User','id','approved_by');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PunchType extends Model
{
    protected $table = 'punch_type';

    protected $fillable = ['name'];
}

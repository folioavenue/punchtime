<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    protected $table = 'shifts';

    protected $fillable  = ['subscription_id','name','grace_time'];

    public function shiftdays(){
        return $this->hasMany('App\ShiftDay','shift','id');
    }

    public function breaks(){
        return $this->hasMany('App\ShiftBreak','shift','id');
    }

    
}

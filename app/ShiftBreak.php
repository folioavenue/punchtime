<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftBreak extends Model
{
    protected $table = 'shift_breaks';

    protected $fillable = ['shift','name','from_time','to_time'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShiftDay extends Model
{
    protected $table = 'shift_days';

    protected $fillable = ['shift','days','shift_start','shift_end'];


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = ['plan_id','plan_expiry_date','company_name','owner_name','email','address','contact_no','company_logo','status'];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    protected $table ='subscription_plans';

    protected $fillable = ['name','description','code','price'];

}

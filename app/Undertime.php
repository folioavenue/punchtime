<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Undertime extends Model
{
    protected $table = 'undertime';

    protected $fillable = ['subscription_id','employee','date','reason','approved_by','status'];

    public function getEmployee()
    {
        return $this->hasOne('App\User','id','employee');
    }

    public function getRequestStatus()
    {
        return $this->hasOne('App\RequestStatus','id','status');
    }

    public function getApproval()
    {
        return $this->hasOne('App\User','id','approved_by');
    }
}

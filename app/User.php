<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'subscription_id','name','birthdate','civil_status','gender','email','username','password','department','position','status','shift','sl_credits','vl_credits','address','contact_no','date_hired','user_type','avatar','user_status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getsubscription()
    {
        return $this->hasOne('App\Subscription','id','subscription_id');
    }

    public function getdepartment()
    {
        return $this->hasOne('App\Department','id','department');
    }

    public function getposition()
    {
        return $this->hasOne('App\Position','id','position');
    }

}

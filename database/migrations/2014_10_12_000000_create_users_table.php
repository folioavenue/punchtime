<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('subscription_id')->nullable();
            $table->string('name');            
            $table->date('birthdate')->nullable();
            $table->integer('gender')->nullable();
            $table->integer('civil_status')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('department')->nullable();
            $table->integer('position')->nullable();
            $table->integer('status')->nullable();
            $table->integer('shift')->nullable();
            $table->integer('sl_credits')->nullable();
            $table->integer('vl_credits')->nullable();
            $table->date('date_hired')->nullable();
            $table->integer('user_type');
            $table->timestamp('email_verified_at')->nullable(); 
            $table->string('username')->nullable();           
            $table->string('password');
            $table->integer('user_status')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('subscription_id')->unsigned();
            $table->integer('employee')->unsigned();
            $table->date('from_date');
            $table->date('to_date');
            $table->string('reason')->nullable();
            $table->integer('leave_type');
            $table->integer('credit_type');
            $table->integer('approved_by')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave');
    }
}

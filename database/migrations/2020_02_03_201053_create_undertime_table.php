<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUndertimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('undertime', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('subscription_id')->unsigned();
            $table->integer('employee')->unsigned();
            $table->date('date');
            $table->string('reason')->nullable();
            $table->integer('approved_by')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('undertime');
    }
}

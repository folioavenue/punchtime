<?php

use Illuminate\Database\Seeder;
use App\CivilStatus;

class CivilStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CivilStatus::create(['name'=>'Single']);
        CivilStatus::create(['name'=>'Married']);
    }
}

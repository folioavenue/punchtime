<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserTypeSeeder::class);
         $this->call(UserTableSeeder::class);
         $this->call(UserStatusTableSeeder::class);
         $this->call(CivilStatusTableSeeder::class);
         $this->call(PunchTypeTableSeeder::class);
         $this->call(GenderTableSeeder::class);
         $this->call(LeaveTypeTableSeeder::class);
         $this->call(LeaveCreditTypeTableSeeder::class);
         $this->call(RequestStatusTableSeeder::class);
         $this->call(SubscriptionPlanTableSeeder::class);

    }
}

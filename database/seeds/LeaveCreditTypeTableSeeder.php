<?php

use Illuminate\Database\Seeder;
use App\LeaveCreditType;

class LeaveCreditTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaveCreditType::create(['name' => 'With Pay']);
        LeaveCreditType::create(['name' => 'Without Pay']);
    }
}

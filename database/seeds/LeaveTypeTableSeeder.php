<?php

use Illuminate\Database\Seeder;
use App\LeaveType;

class LeaveTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LeaveType::create(['name' => 'Maternity']);
        LeaveType::create(['name' => 'Paternity']);
        LeaveType::create(['name' => 'Sick']);
        LeaveType::create(['name' => 'Vacation']);
    }
}

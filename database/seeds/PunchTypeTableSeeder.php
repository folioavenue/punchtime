<?php

use Illuminate\Database\Seeder;
use App\PunchType;
class PunchTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PunchType::create(['name' => 'In']);
        PunchType::create(['name' => 'Out']);
        
    }
}

<?php

use Illuminate\Database\Seeder;
use App\RequestStatus;

class RequestStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RequestStatus::create(['name' => 'Approved','class' => 'badge badge-primary']);
        RequestStatus::create(['name' => 'Disapproved','class' => 'badge badge-danger']);
        RequestStatus::create(['name' => 'Cancelled','class' => 'badge badge-danger']);
        RequestStatus::create(['name' => 'On-Process','class' => 'badge badge-warning']);

    }
}

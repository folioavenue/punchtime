<?php

use Illuminate\Database\Seeder;
use App\SubscriptionPlan;

class SubscriptionPlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubscriptionPlan::create(['name' => 'One Month Free Trial','description' => 'You can access all features of the system for free in one month only!','code'=> '+1 months','price' => 0 ]);
        SubscriptionPlan::create(['name' => 'Bronze','description' => 'You can access all features of the system for 3 months','code'=> '+3 months','price' => 25 ]);
        SubscriptionPlan::create(['name' => 'Silver','description' => 'You can access all features of the system for 6 months','code'=> '+6 months','price' => 50 ]);
        SubscriptionPlan::create(['name' => 'Gold','description' => 'You can access all features of the system for 12 months','code'=> '+12 months','price' => 100 ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use App\UserStatus;
class UserStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        UserStatus::create(['name'=>'Active']);
        UserStatus::create(['name'=>'Inactive']);
        UserStatus::create(['name'=>'Deactivate']);
    }
}

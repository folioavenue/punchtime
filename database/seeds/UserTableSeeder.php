<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'  => 'Superadmin',
            'email' => 'info@punchtime.com',
            'username' => 'superadmin',
            'password' => Hash::make('superadmin123'),
            'user_type' => 1,
            ]);
    }
}

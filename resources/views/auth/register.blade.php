<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Punch Time - Register') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/classic.css') }}" rel="stylesheet">
</head>
<body>   
    <div class="wrapper">
        <main class="main d-flex justify-content-center w-100">
		<div class="container d-flex flex-column">
			<div class="row h-100">
				<div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
					<div class="d-table-cell align-middle">
						<br>
						@include('layouts.alert')
						<div class="text-center mt-4">
							<h1 class="h2">Welcome To Punch Time Subscription Form</h1>
							<p class="lead">
							Fill up all the form required!
							</p>
						</div>

						<div class="card">
							<div class="card-body">
								<div class="m-sm-4">
									<div class="text-center">
										<img src="img/logo.png" alt="Chris Wood" class="img-fluid " width="200" height="200" />
									</div>
									<hr>
									</br>
							<form id="smartwizard-validation" class="wizard wizard-success" method="POST" action="/subscription-payment" role="form">
								@csrf
								<ul>
									<li><a href="#validation-step-1">First Step<br /><small>Company Information</small></a></li>
									<li><a href="#validation-step-2">Second Step<br /><small>Account Details</small></a></li>
									<li><a href="#validation-step-3">Third Step<br /><small>Payment Method</small></a></li>
								</ul>

								<div>
								<div id="validation-step-1">
										<div class="form-group">
											<label class="form-label">Company Name
												<span class="text-danger">*</span>
											</label>
											<input name="company_name" type="text" class="form-control required">
										</div>
										<div class="form-group">
											<label class="form-label">Owner Name
												<span class="text-danger">*</span>
											</label>
											<input name="owner_name" type="text" class="form-control required">
										</div>
										<div class="form-group">
											<label class="form-label">Contact Number
												<span class="text-danger">*</span>
											</label>
											<input name="contact_no" type="text" class="form-control required">
										</div>
										<div class="form-group">
											<label class="form-label">Email
												<span class="text-danger">*</span>
											</label>
											<input name="email" type="text" class="form-control required email">
										</div>
										<div class="form-group mb-0">
											<label class="form-label">Address</label>
											<input name="address" type="text" class="form-control required" >
										</div>
									</div>

									<div id="validation-step-2">
										<div class="form-group">
											<label class="form-label">User name
												<span class="text-danger">*</span>
											</label>
											<input name="username" type="text" class="form-control required">
										</div>
										<div class="form-group">
											<label class="form-label">Password
												<span class="text-danger">*</span>
											</label>
											<input name="password" type="password" class="form-control required">
										</div>
										<div class="form-group mb-0">
											<label class="form-label">Confirm Password
												<span class="text-danger">*</span>
											</label>
											<input name="confirm_password" type="password" class="form-control required">
										</div>
									</div>

									<div id="validation-step-3">
										<div class="form-group mb-0">
											<label class="form-label">Subscription Plan
												<span class="text-danger">*</span>
											</label>
											<select name="plan" id="plan" class="form-control" required>
												<option value="">Choose Plan</option>
												@foreach($subscription_plans as $key => $val)
													<option value="{{ $key }}">{{ $val }}</option>
												@endforeach
											</select>
										</div>
										<br>
										<div class="form-group">
											<label class="custom-control custom-checkbox">
											<input type="checkbox" id="agree" class="custom-control-input">
											<span class="custom-control-label">I agree with the Terms and Conditions</span>
											</label>
										</div>

										
									</div>
								</div>
								</div>
							</form>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<script src="{{ asset('js/app.js') }}" ></script>
<script>
	$(document).ready(function(){
			// $(".btn-submit").attr("disabled", true); 

			var $validationForm = $("#smartwizard-validation");


			$validationForm.validate({
				errorPlacement: function errorPlacement(error, element) {
					$(element).parents(".form-group").append(
						error.addClass("invalid-feedback small d-block")
					)
				},
				highlight: function(element) {
					$(element).addClass("is-invalid");
				},
				unhighlight: function(element) {
					$(element).removeClass("is-invalid");
				},
				rules: {
					"confirm_password": {
						equalTo: "input[name=\"password\"]"
					}
				}
			});
			$validationForm
				.smartWizard({
					theme: "arrows",
					autoAdjustHeight: false,
					backButtonSupport: false,
					useURLhash: false,
					showStepURLhash: false,
					toolbarSettings: {						
						toolbarExtraButtons: [$("<button class=\"btn btn-submit btn-success\" type=\"button\" disabled=\"true\">Finish</button>")]
					}
				})
				.on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
					if (stepDirection === "forward") {
						return $validationForm.valid();
					}
					return true;
				});
			$validationForm.find(".btn-submit").on("click", function() {
				if (!$validationForm.valid()) {
					return;
				}else{
					$('#smartwizard-validation').submit();
				}
				
			});

			
	});

	$(function() {		

			$('#close-session-register').on('click',function(){
				sessionStorage.removeItem('register_success');

						
			});
		});

	$(function(){
		
		$("#agree").on('change',function(){
		
		if($(this).is(":checked")){
			$(".btn-submit").attr('disabled',false);
		}else{
			$(".btn-submit").attr('disabled',true);
		}
		});
	});
</script>
   
</body>
</html>

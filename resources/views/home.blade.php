@extends('layouts.app')

@section('content')

<div class="row">
     @if(Auth::user()->user_type == 3)
        <div class="col-12 col-sm-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-3">
                    <div class="media">                        
                        <div class="media-body" >
                            <input type="hidden" name="punch-in-time" id="punch-in-date-time" value="{{ date('Y-m-d H:i:s') }}"> 
                            <input type="hidden" name="punch-in-time" id="punch-in-time" value="{{ date('h:i A') }}">
                            <div id="punch-in-display-text">

                            </div>
                            @if($check_dtr->punch == 2)
                            <div id="punch-in-display-button">
                                <button class="btn btn-primary btn-block" id="button-punch-in">Punch In</button>
                            </div>
                            @else
                            <div>
                                <span>
                                    YOU PUNCH IN AT {{ $check_dtr->date }}
                                </span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="col-12 col-sm-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-3">
                    <div class="media">
                        
                        <div class="media-body" >
                            <input type="hidden" name="punch-in-date-time" id="punch-in-date-time" value="{{ date('Y-m-d H:i:s') }}"> 
                            <input type="hidden" name="punch-out-time" id="punch-out-time" value="{{ date('h:i A') }}">
                            <div id="punch-out-display-text">
                                
                            </div>
                            @if($check_dtr->punch == 1)
                            <div id="punch-out-display-button">
                                <button class="btn btn-primary btn-block " id="button-punch-out">Punch Out</button>
                            </div>
                            @else
                            <div>
                                <span>
                                    YOU PUNCH OUT AT {{ $check_dtr->date }}
                                </span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
   
    @if(Auth::user()->user_type == 3)
    <div class="row">
        
        <div class="col-12 col-sm-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-4">
                    <div class="media">
                        <div class="d-inline-block mt-2 mr-3">
                            <i class="feather-lg text-warning" data-feather="clock"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="mb-2">10</h3>
                            <div class="mb-0">Monthly Work Hours</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-4">
                    <div class="media">
                        <div class="d-inline-block mt-2 mr-3">
                            <i class="feather-lg text-success" data-feather="user-x"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="mb-2">20</h3>
                            <div class="mb-0">Total Absent Days</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-xl d-flex">
            <div class="card flex-fill">
                <div class="card-body py-4">
                    <div class="media">
                        <div class="d-inline-block mt-2 mr-3">
                            <i class="feather-lg text-danger" data-feather="watch"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="mb-2">30 mins</h3>
                            <div class="mb-0">Total Monthly Lates</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-xl d-none d-flex">
            <div class="card flex-fill">
                <div class="card-body py-4">
                    <div class="media">
                        <div class="d-inline-block mt-2 mr-3">
                            <i class="feather-lg text-info" data-feather="cpu"></i>
                        </div>
                        <div class="media-body">
                            <h3 class="mb-2">20 mins</h3>
                            <div class="mb-0">Total Min Overbreaks</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        
    <div class="col-12 col-lg-8 d-flex">
            <div class="card flex-fill w-100">
            <div class="card-header">
                <h5 class="card-title">FullCalendar</h5>
                <!-- <h6 class="card-subtitle text-muted">Open source JavaScript jQuery plugin for a full-sized, drag & drop event calendar.</h6> -->
            </div>
            <div class="card-body">
                <div id="fullcalendar">

                </div>
            </div>
        </div>

        </div>
        <div class="col-12 col-lg-4 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <!-- <span class="badge badge-info float-right">All</span> -->
                    <h5 class="card-title mb-0"><i class="feather-lg text-danger" data-feather="bell"></i> Announcement Board</h5>
                </div>
                <div class="card-body">
                    <div class="media">
                        <img src="img/avatars/avatar-5.jpg" width="36" height="36" class="rounded-circle mr-2" alt="Ashley Briggs">
                        <div class="media-body">
                            <small class="float-right text-navy">5m ago</small>
                            <strong>Ashley Briggs</strong> started following <strong>Stacie Hall</strong><br />
                            <small class="text-muted">Today 7:51 pm</small><br />

                        </div>
                    </div>

                    <hr />
                    <div class="media">
                        <img src="img/avatars/avatar.jpg" width="36" height="36" class="rounded-circle mr-2" alt="Chris Wood">
                        <div class="media-body">
                            <small class="float-right text-navy">30m ago</small>
                            <strong>Chris Wood</strong> posted something on <strong>Stacie Hall</strong>'s timeline<br />
                            <small class="text-muted">Today 7:21 pm</small>

                            <div class="border text-sm text-muted p-2 mt-1">
                                Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing...
                            </div>
                        </div>
                    </div>

                    <hr />
                    <div class="media">
                        <img src="img/avatars/avatar-4.jpg" width="36" height="36" class="rounded-circle mr-2" alt="Stacie Hall">
                        <div class="media-body">
                            <small class="float-right text-navy">1h ago</small>
                            <strong>Stacie Hall</strong> posted a new blog<br />

                            <small class="text-muted">Today 6:35 pm</small>
                        </div>
                    </div>

                    <hr />
                    <a href="#" class="btn btn-primary btn-block">Load more</a>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <div class="col-12 col-lg-6 col-xl-8 d-flex">
            <div class="card flex-fill">
                <div class="card-header">
                    <div class="card-actions float-right">
                        <div class="dropdown show">
                            <a href="#" data-toggle="dropdown" data-display="static">
    <i class="align-middle" data-feather="more-horizontal"></i>
    </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <h5 class="card-title mb-0">Today's Punch</h5>
                </div>
                <table id="datatables-dashboard-projects" class="table table-striped my-0">
                    <thead>
                        <tr>
                            <th>Employee</th>
                            <th class="d-none d-xl-table-cell">Time</th>
                            <th class="d-none d-xl-table-cell">Punch</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($dtr as $d)
                                <tr>
                                    <td>{{ $d->employee->name }}</td>
                                    <td>{{ \Carbon\Carbon::parse($d->date)->format('h:i A') }}</td>
                                    <td><span class="{{ $d->punch == 1 ? 'badge badge-success' : 'badge badge-danger' }}">{{ $d->punch_type->name }}</span></td>
                                </tr>
                            @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-4 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <div class="card-actions float-right">
                        <div class="dropdown show">
                            <a href="#" data-toggle="dropdown" data-display="static">
    <i class="align-middle" data-feather="more-horizontal"></i>
    </a>

                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a>
                            </div>
                        </div>
                    </div>
                    <h5 class="card-title mb-0">Appointments</h5>
                </div>
                <div class="p-4 bg-light">
                    <h2>You have a meeting today!</h2>
                    <p class="mb-0 text-sm">Your next meeting is in 2 hours. Check your <a href="#">schedule</a> to see the details.</p>
                </div>
                <div class="card-body">
                    <ul class="timeline">
                        <li class="timeline-item">
                            <strong>Chat with Carl and Ashley</strong>
                            <span class="float-right text-muted text-sm">30m ago</span>
                            <p>Nam pretium turpis et arcu. Duis arcu tortor, suscipit eget, imperdiet nec, imperdiet iaculis, ipsum. Sed aliquam ultrices mauris. Integer ante arcu...</p>
                        </li>
                        <li class="timeline-item">
                            <strong>The big launch</strong>
                            <span class="float-right text-muted text-sm">2h ago</span>
                            <p>Sed aliquam ultrices mauris. Integer ante arcu, accumsan a, consectetuer eget, posuere ut, mauris. Praesent adipiscing. Phasellus ullamcorper ipsum rutrum
                                nunc...</p>
                        </li>
                        <li class="timeline-item">
                            <strong>Coffee break</strong>
                            <span class="float-right text-muted text-sm">3h ago</span>
                            <p>Curabitur ligula sapien, tincidunt non, euismod vitae, posuere imperdiet, leo. Maecenas malesuada. Praesent congue erat at massa. Sed cursus turpis vitae
                                tortor...</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


   
    @if(Auth::user()->user_type == 3)
    <!-- BEGIN primary modal -->
           
            <div class="modal fade" id="punch" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title align-self-center">PunchTime</h5>                           
                        </div>
                        <div class="modal-body m-3">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-12 d-flex"> 
                                    
                                    <button class="btn btn-lg btn-primary btn-block" id="button-modal-punch-in">Punch In</button>                                  
                                </div>
                            </div>   
                           
                        </div>                        
                    </div>
                </div>
            </div>
            </form>
            <!-- END primary modal -->
      @endif      

@endsection

@section('custom_js')
    <script type="text/javascript">
        $(function(){

            $('#close-session-register').on('click',function(){
				
				var url = "/subscription/delete-session";
				var data = "register_success";
				$.ajax({
                                    type:"POST",
                                    url:url,
                                    data:{
                                    data:data,                         
                                    _token: "{{ csrf_token() }}",       
                                    },
                                    success:function(result){
                                        console.log('Successfully deleted');
                                    },
                                    error:function(error){
                                        console.log(error);
                                    },
			});
            });

            $('#punch').modal('show');
            $('#punch-in-display-text').hide();
            $('#punch-out-display-text').hide();

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-center",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
                }

            $('#button-punch-in').on('click',function(){
                var punch_in = $('#punch-in-time').val();
                var type = 1;
                var user =  {{ Auth::user()->id }};                
                var url = 'dtr';
                        
                        $.ajax({
                              type:"POST",
                              url:url,
                              data:{
                               user : user,
                               date_punch : punch_in, 
                               type : type,                              
                               _token: "{{ csrf_token() }}",       
                              },
                              success:function(result){
                                    toastr["info"]('YOU PUNCH IN AT <b>'+punch_in+'</b>');
                                    $('#punch-in-display-text').show();
                                    $('#punch-in-display-button').hide();
                                    $('#punch-out-display-text').hide();
                                    $('#punch-out-display-button').show();
                                   
                                    $('#punch-in-display-text').html("<span class='align-self-center w-100'>You're Last Punch-In is at : </span><strong>"+punch_in+"</strong>");
                                    
                              },
                              error:function(error){
                                   console.log(error);
                                    toastr["error"]("Failed to punch in right now.<br/>Pls. contact administrator!");
                              }
                             
                        });
            });

            $('#button-modal-punch-in').on('click',function(){
                $('#punch').modal('hide');
                var punch_in = $('#punch-in-time').val();
                var type = 1;
                var user =  {{ Auth::user()->id }};                
                var url = 'dtr';                        
                        $.ajax({
                              type:"POST",
                              url:url,
                              data:{
                               user : user,
                               date_punch : punch_in, 
                               type : type,                              
                               _token: "{{ csrf_token() }}",       
                              },
                              success:function(result){
                                    toastr["info"]('YOU PUNCH IN AT <b>'+punch_in+'</b>');
                                    $('#punch-in-display-text').show();
                                    $('#punch-in-display-button').hide();
                                    $('#punch-out-display-text').hide();
                                    $('#punch-out-display-button').show();
                                    $('#punch-in-display-text').html("<span class='align-self-center w-100'>You're Last Punch-In is at : </span><strong>"+punch_in+"</strong>");

                              },
                              error:function(error){
                                   console.log(error);
                                    toastr["error"]("Failed to punch in right now.<br/>Pls. contact administrator!");
                              }
                             
                        });
            });

            $('#button-punch-out').on('click',function(){
                var punch_out = $('#punch-out-time').val();
                var type = 2;
                var user =  {{ Auth::user()->id }};                
                var url = 'dtr';
                        
                        $.ajax({
                              type:"POST",
                              url:url,
                              data:{
                               user : user,
                               date_punch : punch_out, 
                               type : type,                              
                               _token: "{{ csrf_token() }}",       
                              },
                              success:function(result){
                                    toastr["info"]('YOU PUNCH OUT AT <b>'+punch_out+'</b>');
                                    $('#punch-out-display-text').show();
                                    $('#punch-in-display-text').hide();
                                    $('#punch-in-display-button').show();
                                    $('#punch-out-display-button').hide();
                                    $('#punch-out-display-text').html("<span class='align-self-center w-100'>You're Last Punch-Out is at : </span><strong>"+punch_out+"</strong>");
                                    
                              },
                              error:function(error){
                                   console.log(error);
                                    toastr["error"]("Failed to punch out right now.<br/>Pls. contact administrator!");
                              }
                             
                        });
            });

            
			$("#fullcalendar").fullCalendar({
				header: {
					left: "prev,next today",
					center: "title",
					right: "month,agendaWeek,agendaDay,listMonth"
				},
				weekNumbers: true,
				eventLimit: true,
				editable: true,
				//events: "https://fullcalendar.io/demo-events.json"
				events: [
					@foreach($leave as $l)
                        @if($l->status == 1)    
                            {
                                allDay: true,
                                title : '{{ $l->getEmployee->name }}'+' file '+' {{ $l->getLeaveType->name }}'+' Leave',
                                start : '{{ $l->from_date }}',
                                end : '{{ $l->to_date }}'+' 24:00:00',	
                            },
                        @endif
					@endforeach

                    @foreach($overtime as $o)
                        @if($o->status == 1)    
                            {
                                allDay: true,
                                title : '{{ $o->getEmployee->name }}'+' file overtime for '+'{{ $o->no_of_hours }}'+' hours' ,
                                start : '{{ $o->date }}',
                                end : '{{ $o->date }}',	
                            },
                        @endif
					@endforeach

                    @foreach($undertime as $u)
                        @if($u->status == 1)    
                            {
                                allDay: true,
                                title : '{{ $u->getEmployee->name }}'+' file undertime',
                                start : '{{ $u->date }}',
                                end : '{{ $u->date }}',	
                            },
                        @endif
					@endforeach
				]
			});
		
        });
    </script>
@endsection



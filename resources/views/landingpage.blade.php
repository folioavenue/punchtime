<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    
        <title>PunchTime</title>
    
        <!-- Fonts -->
        <link rel="icon" href="{{ asset('img/gloves.png') }}">
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
        <!-- Styles -->
        <link href="{{ asset('css/classic.css') }}" rel="stylesheet">
        <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
        <style type="text/javascript">
            .main{
                background-image: url("img/office.jpg");
                background-repeat: no-repeat;
                background-size:cover;
            }    
        </style>
    </head>

<body>
	<div class="wrapper">           

		<div class="main">
            
                <div class="row">
                        <div class="col-12 col-lg-10">
                            <br>
                            <div style="margin-left:30px;">
                                <img src="{{ asset('img/logo.png') }}" alt="Stacie Hall" class="img-fluid " width="200" height="200" />									
                            </div>   
                        </div>  
 
                        <div class="col-12 col-lg-2">
                                <br>
                                <a href="{{ url('/login') }}" class="btn btn-lg btn-outline-success  pullright">Sign In</a>
                                <a href="{{ url('/register') }}" class="btn btn-lg btn-outline-success  pullright">Sign Up</a>
                               
                        </div>    
                    </div>

			<main class="content">
                
				<div class="container-fluid p-0">

					

					<div class="row">
						<div class="col-md-10 col-xl-8 mx-auto">
							<h1 class="text-center">We have a plan for everyone</h1>
							<p class="lead text-center mb-4">Whether you're a business or an individual, 1 month trial no credit card required.</p>

							<div class="row justify-content-center mt-3 mb-2">
								<div class="col-auto">
									<nav class="nav btn-group">
										<a href="#monthly" class="btn btn-outline-success active" data-toggle="tab">Subscription Plan</a>
										
									</nav>
								</div>
							</div>

							<div class="tab-content">
								<div class="tab-pane fade show active" id="monthly">
									<div class="row py-4">
										<div class="col-sm-3 mb-3 mb-md-0">
											<div class="card text-center h-100">
												<div class="card-body d-flex flex-column">
													<div class="mb-3">
														<h5>Free 1-month</h5>
														<span class="display-4">$0</span>
													</div>
													<h6>Includes:</h6>
													<ul class="list-unstyled">
														<li class="mb-2">
															Unlimited Employee
														</li>
														<li class="mb-2">
															5 GB data storage
														</li>
													</ul>
													<div class="mt-auto">
                                                        <a href="{{ url('/register') }}" class="btn btn-lg btn-success">Try it for free</a>
													</div>
												</div>
											</div>
                                        </div>
                                        <div class="col-sm-3 mb-3 mb-md-0">
                                                <div class="card text-center h-100">
                                                    <div class="card-body d-flex flex-column">
                                                        <div class="mb-3">
                                                            <h5>Bronze</h5>
                                                            <span class="display-4">$25</span>
                                                        </div>
                                                        <h6>Includes:</h6>
                                                        <ul class="list-unstyled">
                                                            <li class="mb-2">
                                                                 Unlimited Employee
                                                            </li>
                                                            <li class="mb-2">
                                                                Unlimited Data Settings
                                                            </li>
                                                            <li class="mb-2">
                                                                Unlimited storage
                                                            </li>
                                                        </ul>
                                                        <div class="mt-auto">
                                                            <a href="{{ url('/register') }}" class="btn btn-lg btn-outline-success">Sign up</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
										<div class="col-sm-3 mb-3 mb-md-0">
											<div class="card text-center h-100">
												<div class="card-body d-flex flex-column">
													<div class="mb-3">
														<h5>Silver</h5>
														<span class="display-4">$50</span>
													
													</div>
													<h6>Includes:</h6>
													<ul class="list-unstyled">
                                                        <li class="mb-2">
                                                                Unlimited Employee
                                                        </li>
                                                        <li class="mb-2">
                                                            Unlimited Data Settings
                                                        </li>
														<li class="mb-2">
															Unlimited storage
														</li>
														<li class="mb-2">
															Security policy
														</li>
														<li class="mb-2">
															Weekly backups
														</li>
													</ul>
													<div class="mt-auto">
                                                            <a href="{{ url('/register') }}" class="btn btn-lg btn-outline-success">Sign up</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-3 mb-3 mb-md-0">
											<div class="card text-center h-100">
												<div class="card-body d-flex flex-column">
													<div class="mb-3">
														<h5>Globe</h5>
														<span class="display-4">$100</span>
														
													</div>
													<h6>Includes:</h6>
													<ul class="list-unstyled">
														<li class="mb-2">
															Unlimited users
														</li>
														<li class="mb-2">
															Unlimited projects
														</li>
														<li class="mb-2">
															250 GB storage
														</li>
														<li class="mb-2">
															Priority support
														</li>
														<li class="mb-2">
															Security policy
														</li>
														<li class="mb-2">
															Daily backups
														</li>
														<li class="mb-2">
															Custom CSS
														</li>
													</ul>
													<div class="mt-auto">
                                                    <a href="{{ url('/register') }}" class="btn btn-lg btn-outline-success">Sign up</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="annual">
									<div class="row py-4">
										<div class="col-sm-4 mb-3 mb-md-0">
											<div class="card text-center h-100">
												<div class="card-body d-flex flex-column">
													<div class="mb-4">
														<h5>Free</h5>
														<span class="display-4">$0</span>
													</div>
													<h6>Includes:</h6>
													<ul class="list-unstyled">
														<li class="mb-2">
															1 users
														</li>
														<li class="mb-2">
															5 projects
														</li>
														<li class="mb-2">
															5 GB storage
														</li>
													</ul>
													<div class="mt-auto">
														<a href="#" class="btn btn-lg btn-outline-primary">Sign up</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-4 mb-3 mb-md-0">
											<div class="card text-center h-100">
												<div class="card-body d-flex flex-column">
													<div class="mb-4">
														<h5>Standard</h5>
														<span class="display-4">$199</span>
														<span class="text-small4">/mo</span>
													</div>
													<h6>Includes:</h6>
													<ul class="list-unstyled">
														<li class="mb-2">
															5 users
														</li>
														<li class="mb-2">
															50 projects
														</li>
														<li class="mb-2">
															50 GB storage
														</li>
														<li class="mb-2">
															Security policy
														</li>
														<li class="mb-2">
															Weekly backups
														</li>
													</ul>
													<div class="mt-auto">
														<a href="#" class="btn btn-lg btn-primary">Try it for free</a>
													</div>
												</div>
											</div>
										</div>
										<div class="col-sm-4 mb-3 mb-md-0">
											<div class="card text-center h-100">
												<div class="card-body d-flex flex-column">
													<div class="mb-4">
														<h5>Plus</h5>
														<span class="display-4">$399</span>
														<span>/mo</span>
													</div>
													<h6>Includes:</h6>
													<ul class="list-unstyled">
														<li class="mb-2">
															Unlimited users
														</li>
														<li class="mb-2">
															Unlimited projects
														</li>
														<li class="mb-2">
															250 GB storage
														</li>
														<li class="mb-2">
															Priority support
														</li>
														<li class="mb-2">
															Security policy
														</li>
														<li class="mb-2">
															Daily backups
														</li>
														<li class="mb-2">
															Custom CSS
														</li>
													</ul>
													<div class="mt-auto">
														<a href="#" class="btn btn-lg btn-outline-primary">Try it for free</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<hr />

							
						</div>
					</div>

				</div>
			</main>

			<footer class="footer">
				<div class="container-fluid">
					<div class="row text-muted">
						<div class="col-6 text-left">
							{{-- <ul class="list-inline">
								<li class="list-inline-item">
									<a class="text-muted" href="#">Support</a>
								</li>
								<li class="list-inline-item">
									<a class="text-muted" href="#">Help Center</a>
								</li>
								<li class="list-inline-item">
									<a class="text-muted" href="#">Privacy</a>
								</li>
								<li class="list-inline-item">
									<a class="text-muted" href="#">Terms of Service</a>
								</li>
							</ul> --}}
						</div>
						<div class="col-6 text-right">
							<p class="mb-0">
								&copy; 2019 - <a href="index.html" class="text-muted">PunchTime</a>
							</p>
						</div>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<script src="js/app.js"></script>

</body>

</html>
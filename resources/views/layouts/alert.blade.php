@if($message = session('message'))  
    <div class="alert alert-primary alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="alert-message">
           {{ $message }}
        </div>
    </div>
@elseif($error_message = session('error_message'))
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="alert-message">
            {{ $error_message }}
        </div>
    </div>
 @elseif($warning_message = session('warning_message'))
    <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="alert-message">
           {{ $warning_message }}
        </div>
    </div>    
@endif


@if(\Session::has('register_success'))
<div class="alert alert-primary alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" id="close-session-register" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <div class="alert-message">
           {{ \Session::get('register_success') }}
        </div>
    </div>
@endif    
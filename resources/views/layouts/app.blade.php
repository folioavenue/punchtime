<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $page_title }}</title>

    <!-- Fonts -->
    <link rel="icon" href="{{ asset('img/gloves.png') }}">
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/classic.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.css') }}" rel="stylesheet">
</head>
<body>   
<div class="wrapper">

        @include('layouts.sidebar') 

		<div class="main">

            @include('layouts.mainbar')

			<main class="content">
             <div class="container-fluid p-0">
                @include('layouts.alert')
                <div class="row mb-2 mb-xl-4">
                    <div class="col-auto d-none d-sm-block">
                        <h3>{{ $breadcrumb }}</h3>
                    </div>

                </div>   
				@yield('content')
                </div>
			</main>

			@include('layouts.footer')

		</div>
	</div>

	
       
   
</body>
</html>

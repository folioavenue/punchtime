<nav id="sidebar" class="sidebar">
			<div class="sidebar-content ">
				<div class="text-center" style="margin-top:10px;">
									<img src="{{ asset('img/avatars/avatar.jpg') }}" alt="Stacie Hall" class="img-fluid rounded-circle mb-1" width="90" height="110" />									
									@if(auth::user()->user_type == 3 )
									<h5 class="card-title mb-0" style="color:white;">{{ ucfirst(auth::user()->name) }}</h5>
									<div class="text-muted mb-2"><span data-feather="award"></span>  
										{{ auth::user()->getposition->name }}
									</div>

									<div>
										
										<a class="btn btn-primary btn-sm" href="#"><span data-feather="grid"></span> {{ auth::user()->getdepartment->name }} </a>
									</div>
									@else
									<h5 class="card-title mb-0" style="color:white;">{{ ucfirst(auth::user()->getSubscription->company_name) }}</h5>
									<a class="btn btn-primary btn-sm" href="#"><span data-feather="user"></span> Admin </a>
									@endif
					</div>

				<ul class="sidebar-nav">
					<li class="sidebar-header">
						Pages
					</li>
					<li class="sidebar-item active">
						<a href="{{ url('home') }}"  class="sidebar-link">
						<i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboards</span>
						
						</a>
					</li>
					@if(auth::user()->user_type == 1)
					<li class="sidebar-item active">
						<a href="{{ url('subscription') }}"  class="sidebar-link">
						<i class="align-middle" data-feather="credit-card"></i> <span class="align-middle">Subscription</span>
						
						</a>
					</li>
					@endif
					@if(auth::user()->user_type == 1 || auth::user()->user_type == 2)
							
					<li class="sidebar-item">
									<a href="{{ url('department') }}"  class="sidebar-link ">
						<i class="align-middle" data-feather="grid"></i> <span class="align-middle">Employee Departments</span>
						</a>
					</li>
					<li class="sidebar-item">
									<a href="{{ url('position') }}"  class="sidebar-link ">
						<i class="align-middle" data-feather="award"></i> <span class="align-middle">Employee Positions</span>
						</a>
					</li>
					<li class="sidebar-item">
									<a href="{{ url('status') }}"  class="sidebar-link ">
						<i class="align-middle" data-feather="feather"></i> <span class="align-middle">Employee Status</span>
						</a>
					</li>
					<li class="sidebar-item">
						<a href="#shift" data-toggle="collapse" class="sidebar-link collapsed">
						<i class="align-middle" data-feather="git-branch"></i> <span class="align-middle">Shifts</span>
						</a>
						<ul id="shift" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
							<li class="sidebar-item"><a class="sidebar-link" href="{{ url('shift') }}">Lists</a></li>
							<li class="sidebar-item"><a class="sidebar-link" href="{{ url('shift/create') }}">Add</a></li>
							
						</ul>
					</li>
					<li class="sidebar-item">
						<a href="#pages" data-toggle="collapse" class="sidebar-link collapsed">
						<i class="align-middle" data-feather="users"></i> <span class="align-middle">Employee</span>
						</a>
						<ul id="pages" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
							<li class="sidebar-item"><a class="sidebar-link" href="{{ url('/employee') }}">List</a></li>
							<li class="sidebar-item"><a class="sidebar-link" href="{{ url('/employee/create') }}">Add</a></li>
							
						</ul>
					</li>
					
					<li class="sidebar-item">
									<a href="#auth"  class="sidebar-link ">
						<i class="align-middle" data-feather="file"></i> <span class="align-middle">Reports</span>
						</a>
					</li>
					@endif

					<li class="sidebar-item">
						<a href="#leave" data-toggle="collapse" class="sidebar-link collapsed">
						<i class="align-middle" data-feather="slack"></i> <span class="align-middle">Requests</span>
						</a>
						<ul id="leave" class="sidebar-dropdown list-unstyled collapse " data-parent="#sidebar">
							<li class="sidebar-item"><a class="sidebar-link" href="{{ url('leave') }}">Leave</a></li>
							<li class="sidebar-item"><a class="sidebar-link" href="{{ url('overtime') }}">Overtime</a></li>
							<li class="sidebar-item"><a class="sidebar-link" href="{{ url('undertime') }}">Undertime</a></li>
							
						</ul>
					</li>
					<li class="sidebar-item">
									<a href="{{ url('dtr') }}"  class="sidebar-link ">
						<i class="align-middle" data-feather="book"></i> <span class="align-middle">DTR</span>
						</a>
					</li>
					<li class="sidebar-item">
									<a href="#auth"  class="sidebar-link ">
						<i class="align-middle" data-feather="calendar"></i> <span class="align-middle">Calendar</span>
						</a>
					</li>
				</ul>

				<div class="sidebar-bottom d-none d-lg-block">
					<div class="media">
						<img class="rounded-circle mr-3" src="img/avatars/avatar.jpg" alt="Chris Wood" width="40" height="40">
						<div class="media-body">
							<h5 class="mb-1">{{ ucfirst(Auth::user()->name) }} </h5>
							<div>
								<i class="fas fa-circle text-success"></i> Online
							</div>
						</div>
					</div>
				</div>

			</div>
		</nav>
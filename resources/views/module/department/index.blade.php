@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-12 col-lg-4 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Deparment Settings</h5>
                </div>
                <div class="card-body">
                    @if(isset($department))
                         <form method="POST" action="{{ url('department/'.$department->id) }}">
                         @method('PUT')
                    @else
                         <form method="POST" action="/department">
                    @endif
                         @csrf
                            <label for="deparment">Enter Department</label>
                            <input type="text" class="form-control" id="department" name="department" value="{{ isset($department)?$department->name:null }}" placeholder="Name" required>
                            <br>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>   
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-8 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Department Lists</h5>
                </div>
                <div class="card-body">
                    <table id="table-department" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Date Added</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($departments as $department)
                            <tr>
                                <td><a href="{{ url('department/'.$department->id.'/edit') }}"> {{ $department->name }}</a></td>
                                <td>{{ date_format($department->created_at,'M d,Y') }}</td>
                               
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
             </div>
        </div>


    </div>
@endsection

@section('custom_js')
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-department").DataTable({
				responsive: true
			});
		});
	</script>
@endsection
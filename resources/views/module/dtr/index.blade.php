@extends('layouts.app')

@section('content')

    <div class="row">

        
        <div class="col-12 col-lg-12 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Daily Time Punch Record</h5>
                </div>
                <div class="card-body">
                    <table id="table-dtr" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Employee</th>
                                <th>Date</th>
                                <th>Time</th>
                                <th>Punch</th>                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($dtr as $d)
                            <tr>
                                <td>{{ $d->employee->name }}</td>
                                <td>{{ \Carbon\Carbon::parse($d->date)->format('M d,Y')  }}</td>
                                <td>{{ \Carbon\Carbon::parse($d->date)->format('h:i A') }}</td>
                                <td>
                                    @if($d->punch == 1)
                                    <span class="badge badge-primary"> {{ $d->punch_type->name }} </span>
                                    @else
                                    <span class="badge badge-danger"> {{ $d->punch_type->name }} </span></td> 
                                    @endif                              
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
             </div>
        </div>


    </div>
@endsection

@section('custom_js')
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-dtr").DataTable({
				responsive: true
			});
		});
	</script>
@endsection
@extends('layouts.app')

@section('content')
        @if(isset($employee))
                <form role="form" method="POST" action="{{ url('employee/'.$employee->id) }}" enctype="multipart/form-data">
                @method('PUT')
                <input type="hidden" name="new_avatar" value="{{ $employee->avatar }}">
        @else
                <form role="form" method="POST" action="/employee" enctype="multipart/form-data">
        @endif
                @csrf 
        <div class="row">
        
        <div class="col-12 col-lg-8 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Personal Info</h5>
                </div>
                <div class="card-body"> 
                        <div class="form-group">
                            <label for="inputFirstName">Full Name</label>
                            <input type="text" class="form-control" id="inputFirstName" name="name" value="{{ isset($employee)? $employee->name : '' }}" placeholder="Fullname" required>
                        </div>  
                                               
                        <div class="form-group">
                            <label for="birthdate">Birthdate</label>
                            <div class="input-group date" id="birthdate" data-target-input="nearest">
                            
                            <input type="text" value="{{ isset($employee) ? \Carbon\Carbon::parse(date_create($employee->birthdate))->format('m/d/Y') : null }}" class="form-control datetimepicker-input" name="birthdate" id="birthdate"  placeholder="Date" data-target="birthdate" required/>
                            <div class="input-group-append" data-target="#birthdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                            </div>
                        </div>
                        <div class="form-row">                       
                            <div class="form-group col-md-6">
                                <label for="civil_status">Gender</label>
                                <select name="gender" id="gender" class="form-control">
                                    <option value="">Choose gender</option>
                                    @foreach($gender as $key => $val)                                
                                    @php
                                        $selected = '';
                                        if(isset($employee) && $employee->gender == $key){
                                            $selected = 'selected';
                                        }
                                    @endphp
                                    <option value="{{ $key }}" {{ $selected }}>{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>   

                         <div class="form-group col-md-6">
                            <label for="civil_status">Civil Status</label>
                            <select name="civil_status" id="civil_status" class="form-control">
                                <option value="">Choose civil status</option>
                                @foreach($civil_statuses as $key => $val)                                
                                @php
                                    $selected = '';
                                    if(isset($employee) && $employee->civil_status == $key){
                                        $selected = 'selected';
                                    }
                                @endphp
                                <option value="{{ $key }}" {{ $selected }}>{{ $val }}</option>
                                @endforeach
                            </select>
                        </div>       
                     </div> 

                        <div class="form-group">
                            <label for="inputEmail4">Email</label>
                            <input type="email" class="form-control" id="inputEmail4" name="email" value="{{ isset($employee)? $employee->email : '' }}" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <label for="inputAddress">Address</label>
                            <input type="text" class="form-control" id="inputAddress" name="address" value="{{ isset($employee)? $employee->address : '' }}" placeholder="1234 Main St" required>
                        </div>
                        <div class="form-group">
                            <label for="contactno">Contact No.</label>
                            <input type="text" class="form-control" id="contactno" name="contact_no" value="{{ isset($employee)? $employee->contact_no : '' }}" placeholder="Contact no." required>
                        </div>
                        <br>
                        <h5 class="card-title mb-0">Company Info</h5>
                        <br>
                        <div class="form-row">
                        <div class="form-group col-md-6">
                                <label for="position">Employee Position</label>
                                <select id="position" class="form-control" name="position" required>
                                <option value="">Choose employee position</option>
                                @foreach($positions as $key => $val )
                                @php
                                    $selected = '';
                                    if(isset($employee) && $employee->position == $key){
                                        $selected = 'selected';
                                    }
                                @endphp
                                <option value="{{ $key }}" {{ $selected }}>{{ $val }} </option>
                                @endforeach

                             
                            </select> 
                            </div>
                            <div class="form-group col-md-6">
                                <label for="department">Employee Department</label>
                                <select id="department" class="form-control" name="department" required>
                                <option value="">Choose department</option>
                                @foreach($departments as $key => $val )
                                @php
                                    $selected = '';
                                    if(isset($employee) && $employee->department == $key){
                                        $selected = 'selected';
                                    }
                                @endphp
                                <option value="{{ $key }}" {{ $selected }}>{{ $val }}</option>
                                @endforeach
                            </select> 
                            </div>      
                        </div>
                        <div class="form-row">
                        <div class="form-group col-md-6">
                                <label for="status">Employee Status</label>
                                <select id="status" class="form-control" name="status" required>
                                <option value="">Choose status</option>
                                @foreach($statuses as $key => $val )
                                @php
                                    $selected = '';
                                    if(isset($employee) && $employee->status == $key){
                                        $selected = 'selected';
                                    }
                                @endphp
                                <option value="{{ $key }}" {{ $selected }}>{{ $val }}</option>
                                @endforeach
                            </select> 
                            </div>
                            
                            <div class="form-group col-md-6">
                                <label for="shift">Shift Schedule</label>
                                <select id="shift" class="form-control" name="shift" required>
                                <option value="">Choose shift schedule</option>
                                @foreach($shifts as $key => $val )
                                @php
                                    $selected = '';
                                    if(isset($employee) && $employee->shift == $key){
                                        $selected = 'selected';
                                    }
                                @endphp
                                <option value="{{ $key }}" {{ $selected }}>{{ $val }}</option>
                                @endforeach
                            </select> 
                            </div>
                            <div class="col-12 col-xl-4">
                                <div class="form-group mb-xl-0">
                                    <label class="form-label">Dated Hired</label>
                                    <input class="form-control" type="text" name="datehired" {{ isset($employee)? 'disabled' : '' }} />
                                </div>
                            </div>  
                            <div class="col-12 col-xl-4">
                                <div class="form-group mb-xl-0">
                                    <label class="form-label">SL Credits</label>
                                    <input class="form-control" type="number" name="sl_credits"  value="{{ isset($employee)? $employee->sl_credits : '' }}"  />
                                </div>
                            </div> 
                            <div class="col-12 col-xl-4">
                                <div class="form-group mb-xl-0">
                                    <label class="form-label">VL Credits</label>
                                    <input class="form-control" type="number" name="vl_credits" value="{{ isset($employee)? $employee->vl_credits : '' }}" />
                                </div>
                            </div>
                            
                            
                        </div>
                        <br>
                        <button type="submit" class="btn btn-primary">Save</button>
            
                </div>
            </div>
        </div>
        

        <div class="col-12 col-lg-4 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Account Info</h5>
                </div>
                <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                         <div class="text-center">
                            <img alt="Chris Wood" src="{{ asset('img/avatars/avatar.jpg') }}" class="rounded-circle img-responsive mt-2" width="100" height="100" />
                            <div class="form-group">
                                <label class="form-label w-100">Upload avatar</label>
                                <input type="file" name="avatar">
                                <small>For best results, use an image at least 128px by 128px in .jpg format</small>
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name="username" value="{{ isset($employee)? $employee->username : '' }}" placeholder="Username" {{ isset($employee)? 'disabled' : '' }} required>
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password"  @if(!isset($employee)) required @endif>
                        </div>
                        <div class="form-group">
                            <label for="confirm-password">Confirm Password</label>
                            <input type="password" class="form-control" id="confirm-password" name="cpassword" placeholder="Confirm Password" @if(!isset($employee)) required @endif>
                        </div>
                        @if(auth::user()->user_type == 2)
                           
                                <div class="form-group ">
                                    <label class="form-label">Account Status</label>
                                    <select name="account_status" id="account_status" class="form-control" >                           
                                    
                                     @foreach($user_statuses as $key => $val)
                                       @php
                                            $selected = '';
                                            if(isset($employee) && $employee->user_status == $key){
                                                $selected = 'selected';
                                            }
                                        @endphp
                                        <option value="{{ $key }}" {{ $selected }}>{{ $val }}</option>
                                    
                                       @endforeach 
                                    </select>
                                </div>
                                                 
                            @endif    
                        
                    </div>
                </div>
                </div>
            </div>
        </div>

      

    </div>     
    </form>
@endsection

@section('custom_js')
    <script>
        $(function(){
            $("input[name=\"datehired\"]").daterangepicker({
				singleDatePicker: true,
				showDropdowns: true
			});

            $('#birthdate').datetimepicker({
				format: 'L'
            });
        });
    </script>
@endsection


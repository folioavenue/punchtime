@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12 d-flex">
                <div class="card flex-fill w-100">
                    <div class="card-header">
                        <h5 class="card-title mb-0">Employee List</h5>
                    </div>
                    <div class="card-body">
                    <table id="table-employees" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:5%"></th>
                                    <th>Name</th>
                                    <th>Department</th>
                                    <th>Position</th>
                                    <th>Date Hired</th>
                                    <th></th>
                                
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($employees as $employee)
                                <tr>
                                    <td><img alt="image" class="rounded-circle mr-3" src="{{ asset('img/avatars/avatar.jpg') }}" width="40" height="40"></td>
                                    <td><a href="{{ url('employee/'.$employee->id) }}">{{ $employee->name }}</a></td>
                                    <td>{{ $employee->getdepartment->name }}</td>
                                    <td>{{ $employee->getposition->name }}</td>
                                    <td>{{ \Carbon\Carbon::parse($employee->date_hired)->format('M d,Y') }}</td>
                                    <td><a href="{{ url('employee/'.$employee->id.'/edit') }}"><i data-feather="edit" target="_blank"></i></a></td>
                                </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('custom_js')
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-employees").DataTable({
				responsive: true
			});
		});
	</script>
@endsection
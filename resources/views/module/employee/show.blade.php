@extends('layouts.app')

@section('content')
<div class="row">
						<div class="col-md-4 col-xl-3">
							<div class="card mb-3">
								<div class="card-header">
									<h5 class="card-title mb-0">Profile Details</h5>
								</div>
								<div class="card-body text-center">
									<img src="{{ asset('img/avatars/avatar.jpg') }}" alt="Stacie Hall" class="img-fluid rounded-circle mb-2" width="128" height="128" />
									<h5 class="card-title mb-0">{{ $employee->name }}</h5>
									<div class="text-muted mb-2">{{ $employee->getposition->name }}</div>

									<div>
										
										<a class="btn btn-primary btn-sm" href="#"><span data-feather="grid"></span> {{ $employee->getdepartment->name }} </a>
									</div>
								</div>
								<hr class="my-0" />							
								<div class="card-body">
									<h5 class="h6 card-title">About</h5>
									<ul class="list-unstyled mb-0">
										<li class="mb-1"><span data-feather="home" class="feather-sm mr-1"></span> Lives in <a href="#">{{ $employee->address }}</a></li>

										<li class="mb-1"><span data-feather="mail" class="feather-sm mr-1"></span> Email @ <a href="#">{{ $employee->email }}</a></li>
										<li class="mb-1"><span data-feather="phone-call" class="feather-sm mr-1"></span> Contact to <a href="#">{{ $employee->contact_no }}</a></li>
									</ul>
								</div>
								<hr class="my-0" />
								<div class="card-body">
									<h5 class="h6 card-title">Elsewhere</h5>
									<ul class="list-unstyled mb-0">
										<li class="mb-1"><span class="fas fa-globe fa-fw mr-1"></span> <a href="#">staciehall.co</a></li>
										<li class="mb-1"><span class="fab fa-twitter fa-fw mr-1"></span> <a href="#">Twitter</a></li>
										<li class="mb-1"><span class="fab fa-facebook fa-fw mr-1"></span> <a href="#">Facebook</a></li>
										<li class="mb-1"><span class="fab fa-instagram fa-fw mr-1"></span> <a href="#">Instagram</a></li>
										<li class="mb-1"><span class="fab fa-linkedin fa-fw mr-1"></span> <a href="#">LinkedIn</a></li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-8 col-xl-9">
							<div class="card">
								<div class="card-header">
									<div class="card-actions float-right">
										<div class="dropdown show">
											<a href="#" data-toggle="dropdown" data-display="static">
              <i class="align-middle" data-feather="more-horizontal"></i>
            </a>

											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
											</div>
										</div>
									</div>
									<h5 class="card-title mb-0">Activities</h5>
								</div>
								<div class="card-body h-100">

									<div class="media">
										<img src="{{ asset('img/avatars/avatar.jpg') }}" width="36" height="36" class="rounded-circle mr-2" alt="Ashley Briggs">
										<div class="media-body">
											<small class="float-right text-navy">5m ago</small>
											<strong>Ashley Briggs</strong> started following <strong>Stacie Hall</strong><br />
											<small class="text-muted">Today 7:51 pm</small><br />

										</div>
									</div>								

									<hr />
									<a href="#" class="btn btn-primary btn-block">Load more</a>
								</div>
							</div>
						</div>
					</div>
@endsection
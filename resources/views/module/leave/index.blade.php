@extends('layouts.app')

@section('content')

    <div class="row">       
        <div class="col-12 col-lg-12 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                <div class="row">
                      <div class="col-4">
                        <h5 class="card-title mb-0">Leave Request Lists</h5>
                      </div>  
                      <div class="col-8">
                        @if(Auth::user()->user_type == 3)
                        <button type="button" data-toggle="modal" data-target="#modalleave" class="btn btn-primary float-right"><i data-feather="plus"></i> Request</button>
                        @endif
                        <!-- BEGIN primary modal -->
                        <form method="POST" action="/leave" role="form" id="leave-form">
                        @csrf
                        <div class="modal fade" id="modalleave" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Create New Leave Request</h5>
                                        <button type="button" class="close"  data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body m-3">
                                        <div class="row">
                                            <div class="col-12 col-lg-2"></div>
                                            <div class="col-12 col-lg-4">
                                                <button class="btn btn-primary align-middle btn-block" disabled>SL Credits : <strong>{{ Auth::user()->sl_credits }}</strong></button>
                                            </div>
                                            <div class="col-12 col-lg-4">
                                                <button class="btn btn-primary btn-block" disabled>VL Credits : <strong>{{ Auth::user()->vl_credits }}</strong></button>
                                            </div>
                                            <div class="col-12 col-lg-2"></div>
                                        </div> 
                                    <br>   
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex"> 
                                                <label for="name">Leave Type</label>                                    
                                            </div>
                                        </div>   
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex">  
                                                <select name="leave_type" id="leave_type" class="form-control" required>
                                                    <option value="">Choose leave type</option>
                                                    @foreach($leave_types as $key => $val)
                                                        <option value="{{ $key }}" @if(Auth::user()->gender == 1 && $key == 1) disabled @elseif(Auth::user()->gender == 2 && $key == 2) disabled @endif>{{ $val }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex"> 
                                                <label for="name">Effectivity Date</label>                                    
                                            </div>
                                        </div>  
                                        <div class="row">
                                            
                                            <!-- <div class="col-12 col-lg-6 d-flex">
                                                
                                                <div class="input-group date" id="datetimepicker-from-date" data-target-input="nearest">
                                                
                                                <input type="text" class="form-control datetimepicker-input" name="from_date" id="from-date"  placeholder="From Date" data-target="datetimepicker-from-date" required/>
                                                <div class="input-group-append" data-target="#datetimepicker-from-date" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-lg-6 d-flex">
                                        
                                                <div class="input-group date" id="datetimepicker-to-date" data-target-input="nearest">
                                            
                                                <input type="text" class="form-control datetimepicker-input" name="to_date" id="to-date" placeholder="To Date" data-target="#datetimepicker-time-break-to" required/>
                                                <div class="input-group-append" data-target="#datetimepicker-to-date" data-toggle="datetimepicker">
                                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                </div>
                                                </div>
                                            </div> -->

                                            <div class="col-12 col-lg-12">
                                                <div class="form-group">
                                                    <div class="input-group date" id="daterangepicker-date-range" data-target-input="nearest">
                                                
                                                    <input type="text" class="form-control daterangepicker-input" name="effective_date" id="effective_date" placeholder="To Date" data-target="#daterangepicker-date-range" required/>
                                                    <div class="input-group-append" data-target="#daterangepicker-date-range" data-toggle="daterangepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex"> 
                                                    <label for="name">Reason</label>                                    
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex"> 
                                                    <textarea name="reason" id="reason" cols="30" rows="10" class="form-control" placeholder="Write your reason for leave here..." required></textarea>                                    
                                            </div>
                                        </div>
                                    </div>
                                    
                                            <br>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-12 col-lg-12 d-flex"> 
                                                        <label for="name">Credit Type</label>                                    
                                                    </div>
                                                </div>   
                                                <div class="row">
                                                    <div class="col-12 col-lg-12 d-flex">  
                                                        <select name="leave_credit_type" id="leave_credit_type" class="form-control" required>
                                                            <option value="">Choose credit type</option>
                                                            @foreach($leave_credit_types as $key => $val)
                                                                <option value="{{ $key }}">{{ $val }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="submit" class="btn btn-primary">Submit Request</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <!-- END primary modal -->
                      </div>  
                    </div>
                </div>
                <div class="card-body">
                    <table id="table-leave" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th></th>
                               @if(Auth::user()->user_type != 3) 
                                <th>Employee</th>
                               @endif 
                                <th>Leave Type</th>
                                <th>From Date</th>
                                <th>To Date</th>
                                <th># of Days </th>
                                <th>Reason</th>
                                <!-- <th>Approved By</th> -->
                                <th>Status</th>                                                              
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($leave as $l) 
                                <tr>
                                    <td><a data-toggle="modal" data-target="#leave{{ $l->id }}"><i data-feather="eye" class="text-primary"></i></a></td>
                                  @if(Auth::user()->user_type != 3) 
                                    <td>{{ $l->getEmployee->name }}</td>
                                  @endif   
                                    <td>{{ $l->getLeaveType->name }}</td>
                                    <td>{{ \Carbon\Carbon::parse($l->from_date)->format('M d,Y') }}</td>
                                    <td>{{ \Carbon\Carbon::parse($l->to_date)->format('M d,Y') }}</td>
                                    <td>
                                        @php
                                            $from_date = New \DateTime($l->from_date.' 0:00:00');
                                            $to_date = New \DateTime($l->to_date.' 24:00:00');
                                            $diff_in_days = $from_date->diff($to_date);    
                                        @endphp
                                        {{ $diff_in_days->format('%a') }} Days</td>
                                    <td>{{ strlen($l->reason) > 20 ? substr($l->reason,0,20)."..." : substr($l->reason,0,20)}}</td>
                                    <!-- <td>{{ $l->approved_by }}</td> -->
                                    <td id="status{{$l->id}}">
                                        <span class="{{ $l->getRequestStatus->class }}" >{{ $l->getRequestStatus->name }}</span>
                                        @if($l->status == 4 && Auth::user()->user_type == 3)
                                        <button class="btn btn-danger btn-sm" value="{{ $l->id }}" onClick="cancel(this.value,{{ $diff_in_days->format('%a') }});">Cancel</button>
                                        @endif
                                    </td>
                                </tr> 
                                <div class="modal fade" id="leave{{ $l->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <!-- <div class="modal-header">
                                                <h5 class="modal-title text-primary">Leave Request</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div> -->
                                            <div class="modal-body m-3">
                                                <div class="row">
                                                    
                                                        <div class="card-body m-sm-12 m-md-12">
                                                           <div class="row">
                                                               <div class="col-md-12">
                                                                   <h3 class="text-center text-primary">Leave Request</h3>
                                                               </div>
                                                           </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                <h4>{{ $l->getEmployee->name }}</h4>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="text-muted">Department</div>
                                                                    <div class="text-muted">Position</div>
                                                                    
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                    <strong>{{ $l->getEmployee->getdepartment->name }}</strong>
                                                                    <br>
                                                                    <strong>{{ $l->getEmployee->getposition->name }}</strong>
                                                                </div>
                                                            </div>
                                                            <hr class="my-4" />           
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="text-muted">Date Filed</div>
                                                                    <strong>{{ \Carbon\Carbon::parse($l->created_at)->format('M d,Y') }}</strong>
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                    <div class="text-muted">Effectivity Date</div>
                                                                    <strong> {{ \Carbon\Carbon::parse($l->from_date)->format('M d,Y')}} - {{ \Carbon\Carbon::parse($l->to_date)->format('M d,Y')}}</strong>
                                                                </div>
                                                            </div>
                                                                <br>
                                                            <div class="row mb-12">
                                                                <div class="col-md-6">
                                                                    <div class="text-muted">Reason</div>
                                                                   
                                                                    <p>
                                                                        <strong>{{ $l->reason }}</strong>
                                                                    </p>
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                    <div class="text-muted">Leave Type</div>
                                                                    <strong>
                                                                    {{ $l->getLeaveType->name }} 
                                                                    </strong>
                                                                    <div class="text-muted">Credit Type</div>
                                                                    <strong>
                                                                    {{ $l->getCreditType->name }}
                                                                    </strong>
                                                                </div>
                                                               
                                                            </div>

                                                            <hr class="my-4" />

                                                            <div class="row mb-4">
                                                                <div class="col-md-6">
                                                                    <div class="text-muted">Status</div>
                                                                    <strong id="statusmodal{{$l->id}}">
                                                                    <span class="{{ $l->getRequestStatus->class }}">{{ $l->getRequestStatus->name }} </span>
                                                                    </strong>
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                @if($l->approved_by != null)  
                                                                    @if($l->approved_by == Auth::user()->id)
                                                                        <div class="text-muted">
                                                                            @if($l->status == 1)
                                                                                Approved By
                                                                            @elseif($l->status == 2)
                                                                                Disapproved By
                                                                            @endif
                                                                            </div>
                                                                            <strong>
                                                                                You
                                                                            </strong>
                                                                        </div> 
                                                                    @else
                                                                         <div class="text-muted">
                                                                         @if($l->status == 1)
                                                                                Approved By
                                                                            @elseif($l->status == 2)
                                                                                Disapproved By
                                                                            @endif
                                                                         </div>
                                                                            <strong>
                                                                                {{ $l->getApproval->name }}
                                                                            </strong>
                                                                        </div>    
                                                                    @endif
                                                                @else
                                                                        <div class="text-muted"></div>
                                                                            <strong>
                                                                                
                                                                            </strong>
                                                                        </div> 
                                                                @endif  
                                                            </div>
                                                            @if(Auth::user()->user_type !=3 && ($l->status == 4))
                                                            <br>
                                                            <div class="row mb-4" id="button-approval{{$l->id}}">
                                                                <div class="col-md-2"></div>
                                                                <div class="col-md-4">
                                                                    <input type="hidden" name="leave-id" id="leave-id{{$l->id}}" value="{{ $l->id }}">
                                                                    <button class="btn btn-primary align-middle" value="1" onClick="update(this.value,{{$l->id}},{{ $diff_in_days->format('%a') }});"><i data-feather="thumbs-up"></i> Approved</button>
                                                                </div>
                                                                <div class="col-md-4 text-md-right">
                                                                    <button class="btn btn-danger align-middle"  value="2" onClick="update(this.value,{{$l->id}},{{ $diff_in_days->format('%a') }});"><i data-feather="thumbs-down"></i> Disapproved</button>
                                                                </div>  
                                                                <div class="col-md-2"></div>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    
                                                </div>
                                             </div>
                                            <!-- <div class="modal-footer">                                               
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                                                
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
             </div>
        </div>


    </div>
@endsection

@section('custom_js')
<script src="{{ asset('js/sweetalert.min.js') }}" ></script>
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-leave").DataTable({
				responsive: true
			});

            // $('#datetimepicker-from-date').datetimepicker({
			// 	format: 'L'
            // });
            
            // $('#datetimepicker-to-date').datetimepicker({
			// 	format: 'L'
            // });

            $('input[name=\"effective_date\"]').daterangepicker({
				opens: "right",
                minDate: new Date()
			});

            
		});

       function cancel(id,no_of_days){
         
           var url = '/leave/cancel-request';
             $.ajax({
                    type:"POST",
                    url:url,
                    data:{
                    id:id,          
                    no_of_days:no_of_days,                   
                    _token: "{{ csrf_token() }}",       
                    },
                    success:function(result){
                        toastr["info"](result);
                        $("#status"+id).html("<span id='statustext"+id+"'>Cancelled</span>");
                        $("#statustext"+id).addClass("badge badge-danger");

                        $("#statusmodal"+id).html("<span id='statusmodaltext"+id+"'>Cancelled</span>");
                        $("#statusmodaltext"+id).addClass("badge badge-danger");
                        
                    },
                    error:function(error){
                        console.log(error);
                        toastr["error"](error);
                    }
                    
            });

         
            
          }  

          function update(status_id,leave_id,no_of_days){
             
            var url = '/leave/update-request';
            
            var message = "";
            var word = "";

            if(parseInt(status_id) == 1){
                message = "Yes, approve it!";
                word = "approved";
            }else{
           
                message = "Yes, disapproved it!";
                word = "disapproved";
            }
            swal({
                        title: "",
                        text: "Are you sure want to "+word+" this leave request?",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: message,
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true },
                    function (isConfirm) {
                        if(isConfirm){
                             $.ajax({
                                    type:"POST",
                                    url:url,
                                    data:{
                                    id:leave_id,
                                    status:status_id,  
                                    no_of_days:no_of_days,                          
                                    _token: "{{ csrf_token() }}",       
                                    },
                                    success:function(result){
                                        toastr["info"](result);
                                        if(parseInt(status_id) == 1){
                                            $("#status"+leave_id).html("<span id='statustext"+leave_id+"'>Approved</span>");
                                            $("#statustext"+leave_id).addClass("badge badge-primary");

                                            $("#statusmodal"+leave_id).html("<span id='statusmodaltext"+leave_id+"'>Approved</span>");
                                            $("#statusmodaltext"+leave_id).addClass("badge badge-primary");    
                                            $("#leave"+leave_id).modal('hide');
                                            $("#button-approval"+leave_id).hide();
                                        }else{   
                                            $("#leave"+leave_id).modal('hide');
                                            $("#button-approval"+leave_id).hide();
                                            $("#status"+leave_id).html("<span id='statustext"+leave_id+"'>Disapproved</span>");
                                            $("#statustext"+leave_id).addClass("badge badge-danger");

                                            $("#statusmodal"+leave_id).html("<span id='statusmodaltext"+leave_id+"'>Disapproved</span>");
                                            $("#statusmodaltext"+leave_id).addClass("badge badge-danger");    
                                        }                    
                                    },
                                    error:function(error){
                                        console.log(error);
                                        toastr["error"](error);
                                    }
                                    
                            });
                        }
                    });
        
       }
	</script>
@endsection
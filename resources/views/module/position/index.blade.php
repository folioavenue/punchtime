@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-12 col-lg-4 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Position Settings</h5>
                </div>
                <div class="card-body">
                    @if(isset($position))
                         <form method="POST" action="{{ url('position/'.$position->id) }}">
                         @method('PUT')
                    @else
                         <form method="POST" action="/position">
                    @endif
                         @csrf
                            <label for="position">Enter Position</label>
                            <input type="text" class="form-control" id="position" name="position" value="{{ isset($position)?$position->name:null }}" placeholder="Name" required>
                            <br>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>   
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-8 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Position Lists</h5>
                </div>
                <div class="card-body">
                    <table id="table-position" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Date Added</th>                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($positions as $position)
                            <tr>
                                <td><a href="{{ url('position/'.$position->id.'/edit') }}"> {{ $position->name }}</a></td>
                                <td>{{ date_format($position->created_at,'M d,Y') }}</td>                               
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
             </div>
        </div>


    </div>
@endsection

@section('custom_js')
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-position").DataTable({
				responsive: true
			});
		});
	</script>
@endsection
@extends('layouts.app')

@section('content')
    @if(isset($shift))
            <form method="POST" action="{{ url('shift/'.$shift->id) }}">
            @method('PUT')
    @else
            <form method="POST" action="/shift">
    @endif
            @csrf
    <div class="row">       
        <div class="col-12 col-lg-12 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <div class="row">
                      <div class="col-12 col-lg-4">
                            <h5 class="card-title mb-0">Shift Settings</h5>
                      </div>
                      <div class="col-12 col-lg-8">
                            <div class="form-group">
                                <label class="custom-control custom-checkbox float-right">
                                    <input type="checkbox" class="custom-control-input" name="option" id="option">
                                    <span class="custom-control-label">Use Custom Field</span>
                                </label>
                            </div>
                      </div>                      
                    </div>
                </div>
                <div class="card-body">
                  
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-2 d-flex"> 
                                    <label for="name">Name</label>                                    
                                </div>
                                <div class="col-12 col-lg-8 d-flex">                                     
                                    <input type="text" class="form-control" id="name" name="name" value="{{ isset($shift) ? $shift->name : '' }}" placeholder="Shift Name" required>
                                </div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-2 d-flex"> 
                                    <label for="name">Grace Time Period</label>                                    
                                </div>
                                <div class="col-12 col-lg-8 d-flex">                                     
                                    <input type="number" class="form-control" id="grace_time" name="grace_time" value="{{ isset($shift) ? $shift->grace_time : '' }}" placeholder="Grace Time, inputs must be minute value.." required>
                                </div>
                            </div>
                        </div>  
                        <div id="advance-field">
                        <div class="form-group" >
                            <div class="row">
                                 <div class="col-12 col-lg-2 d-flex"> 
                                    <label for="name">Select Days</label>                                    
                                </div>                                
                            
                                <div class="col-12 col-lg-4 d-flex"> 
                                      <select name="from_day" id="from_day" class="form-control" >
                                        <option value="">From day</option>
                                        <option value="1">Monday</option>
                                        <option value="2">Tuesday</option>
                                        <option value="3">Wednesday</option>
                                        <option value="4">Thursday</option>
                                        <option value="5">Friday</option>
                                        <option value="6">Saturday</option>
                                        <option value="7">Sunday</option>
                                      </select>                                
                                </div>
                               

                               
                                <div class="col-12 col-lg-4 d-flex"> 
                                      <select name="to_day" id="to_day" class="form-control" >
                                        <option value="">To day</option>
                                        <option value="1">Monday</option>
                                        <option value="2">Tuesday</option>
                                        <option value="3">Wednesday</option>
                                        <option value="4">Thursday</option>
                                        <option value="5">Friday</option>
                                        <option value="6">Saturday</option>
                                        <option value="7">Sunday</option>
                                      </select>                                
                                </div>    
                            </div>
                            </div> 
                            <div class="form-group" > 
                            <div class="row">
                                
                                <div class="col-12 col-lg-2 d-flex">   
                                    <label class="form-label">Select Time</label>
                                </div>  
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-from-time" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="from_time" value="{{ isset($shift)  && $advance_time != null ? \Carbon\Carbon::parse($advance_time->shift_start)->format('h:i A') : null }}" placeholder="From Time" data-target="datetimepicker-to-time" />
                                    <div class="input-group-append" data-target="#datetimepicker-from-time" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-to-time" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="to_time" value="{{ isset($shift)  && $advance_time != null ? \Carbon\Carbon::parse($advance_time->shift_end)->format('h:i A') : null }}" placeholder="To Time" data-target="#datetimepicker-to-time" />
                                    <div class="input-group-append" data-target="#datetimepicker-to-time" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div id="custom-field">
                        <div class="form-group" > 
                            <div class="row">
                                
                                <div class="col-12 col-lg-2 d-flex">   
                                    <label class="form-label">Monday Schedule</label>
                                </div>  
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-monday-from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="monday_from" value="{{ isset($shift)  && $monday->shift_start != null ? \Carbon\Carbon::parse($monday->shift_start)->format('h:i A') : null }}" placeholder="From Time" data-target="datetimepicker-time-monday-from" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-monday-from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-monday-to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="monday_to" value="{{ isset($shift)  && $monday->shift_end != null ? \Carbon\Carbon::parse($monday->shift_end)->format('h:i A') : null }}" placeholder="To Time" data-target="#datetimepicker-time-monday-to" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-monday-to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-2 d-flex">   
                                    <label class="form-label">Tuesday Schedule</label>
                                </div>  
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-tuesday-from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="tuesday_from" value="{{ isset($shift) && $tuesday->shift_start != null ? \Carbon\Carbon::parse($tuesday->shift_start)->format('h:i A') : null }}" placeholder="From Time" data-target="datetimepicker-time-tuesday-from" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-tuesday-from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-tuesday-to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="tuesday_to" value="{{ isset($shift) && $tuesday->shift_end != null ? \Carbon\Carbon::parse($tuesday->shift_end)->format('h:i A') : null }}" placeholder="To Time" data-target="#datetimepicker-time-tuesday-to" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-tuesday-to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-2 d-flex">   
                                    <label class="form-label">Wednesday Schedule</label>
                                </div>  
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-wednesday-from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="wednesday_from" value="{{ isset($shift)  && $wednesday->shift_start != null ? \Carbon\Carbon::parse($wednesday->shift_start)->format('h:i A') : null }}" placeholder="From Time"  data-target="datetimepicker-time-wednesday-from" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-wednesday-from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-wednesday-to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="wednesday_to" value="{{ isset($shift)  && $wednesday->shift_end != null ? \Carbon\Carbon::parse($wednesday->shift_end)->format('h:i A') : null }}" placeholder="To Time" data-target="#datetimepicker-time-wednesday-to" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-wednesday-to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-2 d-flex">   
                                    <label class="form-label">Thursday Schedule</label>
                                </div>  
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-thursday-from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="thursday_from" value="{{ isset($shift)  && $thursday->shift_start != null ? \Carbon\Carbon::parse($thursday->shift_start)->format('h:i A') : null }}" placeholder="From Time" data-target="datetimepicker-time-thursday-from" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-thursday-from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-thursday-to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="thursday_to" value="{{ isset($shift)  && $thursday->shift_end != null ? \Carbon\Carbon::parse($thursday->shift_end)->format('h:i A') : null }}" placeholder="To Time" data-target="#datetimepicker-time-thursday-to" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-thursday-to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-2 d-flex">   
                                    <label class="form-label">Friday Schedule</label>
                                </div>  
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-friday-from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="friday_from" value="{{ isset($shift)  && $friday->shift_start != null ? \Carbon\Carbon::parse($friday->shift_start)->format('h:i A') : null }}" placeholder="From Time" data-target="datetimepicker-time-friday-from" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-friday-from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-friday-to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="friday_to" value="{{ isset($shift)  && $friday->shift_end != null ? \Carbon\Carbon::parse($friday->shift_end)->format('h:i A') : null }}" placeholder="To Time" data-target="#datetimepicker-time-friday-to" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-friday-to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-2 d-flex">   
                                    <label class="form-label">Saturday Schedule</label>
                                </div>  
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-saturday-from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="saturday_from" value="{{ isset($shift)  && $saturday->shift_start != null ? \Carbon\Carbon::parse($saturday->shift_start)->format('h:i A') : null }}" placeholder="From Time" data-target="datetimepicker-time-saturday-from" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-saturday-from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-saturday-to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="saturday_to" value="{{ isset($shift)  && $saturday->shift_end != null ? \Carbon\Carbon::parse($saturday->shift_end)->format('h:i A') : null }}" placeholder="To Time" data-target="#datetimepicker-time-saturday-to" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-saturday-to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-12 col-lg-2 d-flex">   
                                    <label class="form-label">Sunday Schedule</label>
                                </div>  
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-sunday-from" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="sunday_from" value="{{ isset($shift)  && $sunday->shift_start != null ? \Carbon\Carbon::parse($sunday->shift_start)->format('h:i A') : null }}" placeholder="From Time" data-target="datetimepicker-time-sunday-from" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-sunday-from" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4 d-flex">
                                    <div class="input-group date" id="datetimepicker-time-sunday-to" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" name="sunday_to" value="{{ isset($shift) && $sunday->shift_end != null ? \Carbon\Carbon::parse($sunday->shift_end)->format('h:i A') : null }}" placeholder="To Time" data-target="#datetimepicker-time-sunday-to" />
                                    <div class="input-group-append" data-target="#datetimepicker-time-sunday-to" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        </div>
                        <div class="row">
                            <div class="col-12 col-lg-10 d-flex">   
                                <button type="submit" class="btn btn-primary float-right">Save</button>
                            </div>
                        </div>
                    
                      
                </div>
             </div>
        </div>
        </form>

     @if(isset($shift))   
        <div class="col-12 col-lg-12 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <div class="row">
                      <div class="col-4">
                        <h5 class="card-title mb-0">Break Time Schedule Lists</h5>
                      </div>  
                      <div class="col-8">
                        <button type="button" data-toggle="modal" data-target="#modalbreak" class="btn btn-primary float-right">Add</button>
                        <!-- BEGIN primary modal -->
                        <form method="POST" action="/shift/break-schedule">
                        @csrf
                        <div class="modal fade" id="modalbreak" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Add Break Time Schedule</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body m-3">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex"> 
                                                <label for="name">Break Schedule Name</label>                                    
                                            </div>
                                        </div>   
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex">  
                                                <input type="hidden" name="shift" value="{{ isset($shift) ? $shift->id : null }}">                              
                                                <input type="text" class="form-control" id="name" name="name"  placeholder="e.g Lunch Break" required>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                            <div class="row">
                                                
                                                <div class="col-12 col-lg-6 d-flex">
                                                    
                                                    <div class="input-group date" id="datetimepicker-time-break-from" data-target-input="nearest">
                                                    
                                                    <input type="text" class="form-control datetimepicker-input" name="break_from"  placeholder="From Time" data-target="datetimepicker-time-break-from" />
                                                    <div class="input-group-append" data-target="#datetimepicker-time-break-from" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-lg-6 d-flex">
                                            
                                                    <div class="input-group date" id="datetimepicker-time-break-to" data-target-input="nearest">
                                                
                                                    <input type="text" class="form-control datetimepicker-input" name="break_to"  placeholder="To Time" data-target="#datetimepicker-time-break-to" />
                                                    <div class="input-group-append" data-target="#datetimepicker-time-break-to" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Add</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <!-- END primary modal -->
                      </div>  
                    </div>
                </div>
                <div class="card-body">
                   
                    <table id="table-break" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Break Start</th>
                                <th>Break End</th>                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($shift->breaks as $break)
                                <tr>
                                    <td>{{ $break->name }}</td>
                                    <td><span class="badge badge-primary"> {{ \Carbon\Carbon::parse($break->from_time)->format('h:i A') }} </span></td>
                                    <td><span class="badge badge-primary"> {{ \Carbon\Carbon::parse($break->to_time)->format('h:i A') }} </span></td>
                                </tr>
                            @endforeach                            
                        </tbody>
                    </table>
                </div>
             </div>
        </div>
    @endif

    </div>
@endsection

@section('custom_js')
    <script>
        $(document).ready(function(){
           
        });
		$(function() {

            $("#custom-field").hide();
            $("#option").on('change',function(){
                if(this.checked){
                    $("#custom-field").show(1000);
                    $("#advance-field").hide(1000);
                }else{
                    $("#custom-field").hide(1000);
                    $("#advance-field").show(1000);
                }    
            });

			// Datatables Responsive
			$("#table-shift").DataTable({
				responsive: true
            });

            $("#table-break").DataTable({
				responsive: true
            });

            $('#datetimepicker-from-time').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-to-time').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-monday-from').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-monday-to').datetimepicker({
				format: 'LT'
            });
            $('#datetimepicker-time-tuesday-from').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-tuesday-to').datetimepicker({
				format: 'LT'
            });
            $('#datetimepicker-time-wednesday-from').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-wednesday-to').datetimepicker({
				format: 'LT'
            });
            $('#datetimepicker-time-thursday-from').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-thursday-to').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-friday-from').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-friday-to').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-saturday-from').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-saturday-to').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-sunday-from').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-sunday-to').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-break-from').datetimepicker({
				format: 'LT'
            });
            
            $('#datetimepicker-time-break-to').datetimepicker({
				format: 'LT'
			});

           
		});
	</script>
@endsection
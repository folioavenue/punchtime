@extends('layouts.app')

@section('content')

    <div class="row">       
        <div class="col-12 col-lg-12 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Shift Lists</h5>
                </div>
                <div class="card-body">
                    <table id="table-shift" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Monday</th>
                                <th>Tuesday</th>
                                <th>Wednesday</th>
                                <th>Thursday</th>
                                <th>Friday</th>
                                <th>Saturday</th>
                                <th>Sunday</th>                               
                            </tr>
                        </thead>
                        <tbody>
                              @foreach($shifts as $shift)
                                 <tr>
                                     <td><a href="{{ url('shift/'.$shift->id.'/edit') }}">{{ $shift->name }} </a></td>
                                     @foreach($shift->shiftdays as $s)
                                     @if($s->shift_start != null && $s->shift_end != null)
                                         <td><span class="badge badge-primary"> {{ \Carbon\Carbon::parse($s->shift_start)->format('h:i A').' - '.\Carbon\Carbon::parse($s->shift_end)->format('h:i A') }} </span></td>
                                     @else
                                         <td><span class="badge badge-danger"> No Sched </span></td>
                                     @endif
                                     @endforeach
                                 </tr>
                              @endforeach
                            
                        </tbody>
                    </table>
                </div>
             </div>
        </div>


    </div>
@endsection

@section('custom_js')
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-shift").DataTable({
				responsive: true
			});
		});
	</script>
@endsection
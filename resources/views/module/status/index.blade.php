@extends('layouts.app')

@section('content')

    <div class="row">

        <div class="col-12 col-lg-4 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Status Settings</h5>
                </div>
                <div class="card-body">
                    @if(isset($status))
                         <form method="POST" action="{{ url('status/'.$status->id) }}">
                         @method('PUT')
                    @else
                         <form method="POST" action="/status">
                    @endif
                         @csrf
                            <label for="status">Enter Status</label>
                            <input type="text" class="form-control" id="status" name="status" value="{{ isset($status)?$status->name:null }}" placeholder="Name" required>
                            <br>
                            <button type="submit" class="btn btn-primary">Save</button>
                        </form>   
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-8 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                    <h5 class="card-title mb-0">Status Lists</h5>
                </div>
                <div class="card-body">
                    <table id="table-status" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Date Added</th>                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($statuses as $status)
                            <tr>
                                <td><a href="{{ url('status/'.$status->id.'/edit') }}"> {{ $status->name }}</a></td>
                                <td>{{ date_format($status->created_at,'M d,Y') }}</td>                               
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
             </div>
        </div>


    </div>
@endsection

@section('custom_js')
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-status").DataTable({
				responsive: true
			});
		});
	</script>
@endsection
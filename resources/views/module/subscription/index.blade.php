@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12 col-lg-12 d-flex">
                <div class="card flex-fill w-100">
                    <div class="card-header">
                        <h5 class="card-title mb-0">Subscription Lists</h5>
                    </div>
                    <div class="card-body">
                    <table id="table-subscription" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th style="width:5%"></th>
                                    <th>Company</th>
                                    <th>Owner</th>
                                    <th>Plan</th>
                                    <th>Expiry Date</th>
                                    <th>Payment</th>   
                                    <th>Status</th>                             
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($subscriptions as $subscription)
                                    <td></td>
                                    <td>{{ $subscription->company_name}}</td>
                                    <td>{{ $subscription->owner_name }}</td>
                                    <td></td>
                                    <td></td>
                                    <td>$100,000</td>
                                    <td> @if($subscription->status == 1) <span class="badge badge-success">Active</span> @else <span class="badge badge-danger">Deactivated</span> @endif </td>
                                    
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
@endsection

@section('custom_js')
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-subscription").DataTable({
				responsive: true
			});
		});
	</script>
@endsection
@extends('layouts.app')

@section('content')

    <div class="row">       
        <div class="col-12 col-lg-12 d-flex">
            <div class="card flex-fill w-100">
                <div class="card-header">
                <div class="row">
                      <div class="col-4">
                        <h5 class="card-title mb-0">Undertime Request Lists</h5>
                      </div>  
                      <div class="col-8">
                      @if(Auth::user()->user_type == 3)  
                        <button type="button" data-toggle="modal" data-target="#modalundertime" class="btn btn-primary float-right"><i data-feather="plus"></i> Request</button>
                       @endif
                        <!-- BEGIN primary modal -->
                        <form method="POST" action="/undertime" role="form" id="undertime-form">
                        @csrf
                        <div class="modal fade" id="modalundertime" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Create New Undertime Request</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body m-3">
                                    <div class="form-group">
                                            <div class="row">
                                                <div class="col-12 col-lg-12 d-flex"> 
                                                    <label for="name">Date</label>                                    
                                                </div>
                                            </div>  
                                            <div class="row">
                                                
                                                <div class="col-12 col-lg-12 d-flex">
                                                    
                                                    <div class="input-group date" id="datetimepicker-date" data-target-input="nearest">
                                                    
                                                    <input type="text" class="form-control datetimepicker-input" name="date" id="date"  placeholder="Date" data-target="datetimepicker-date" required/>
                                                    <div class="input-group-append" data-target="#datetimepicker-date" data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                                               
                                        </div>                                      
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex"> 
                                                    <label for="name">Reason</label>                                    
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-lg-12 d-flex"> 
                                                    <textarea name="reason" id="reason" cols="30" rows="10" class="form-control" placeholder="Write your reason for undertime here..." required></textarea>                                    
                                            </div>
                                        </div>
                                    </div>
                                  
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="submit" class="btn btn-primary">Submit Request</button>
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                        <!-- END primary modal -->
                      </div>  
                    </div>
                </div>
                <div class="card-body">
                    <table id="table-undertime" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th></th>
                               @if(Auth::user()->user_type != 3) 
                                <th>Employee</th>
                               @endif                                
                                <th>Date</th>
                                <th>Reason</th>
                                <!-- <th>Approved/Disapproved By</th> -->
                                <th>Status</th>                                                              
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($undertime as $u) 
                                <tr>
                                    <td><a data-toggle="modal" data-target="#undertime{{ $u->id }}"><i data-feather="eye" class="text-primary"></i></a></td>
                                  @if(Auth::user()->user_type != 3) 
                                    <td>{{ $u->getEmployee->name }}</td>
                                  @endif                                       
                                    <td>{{ \Carbon\Carbon::parse($u->date)->format('M d,Y') }}</td>
                                    
                                    <td>{{ strlen($u->reason) > 20 ? substr($u->reason,0,20)."..." : substr($u->reason,0,20)}}</td>
                                    <!-- <td>{{ $u->approved_by != null ? $u->approved_by == Auth::user()->id ? "You" : $u->getApproval->name : '' }}</td> -->
                                    <td id="status{{$u->id}}">
                                        <span class="{{ $u->getRequestStatus->class }}" >{{ $u->getRequestStatus->name }}</span>
                                        @if($u->status == 4  && Auth::user()->user_type == 3)
                                        <button class="btn btn-danger btn-sm" value="{{ $u->id }}" onClick="cancel(this.value);">Cancel</button>
                                        @endif
                                    </td>
                                </tr> 
                                <div class="modal fade" id="undertime{{ $u->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <!-- <div class="modal-header">
                                                <h5 class="modal-title text-primary">Overtime Request</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div> -->
                                            <div class="modal-body m-3">
                                                <div class="row">
                                                    
                                                        <div class="card-body m-sm-12 m-md-12">
                                                           <div class="row">
                                                               <div class="col-md-12">
                                                                   <h3 class="text-center text-primary">Undertime Request</h3>
                                                               </div>
                                                           </div>
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                <h4>{{ $u->getEmployee->name }}</h4>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="text-muted">Department</div>
                                                                    <div class="text-muted">Position</div>
                                                                    
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                    <strong>{{ $u->getEmployee->getdepartment->name }}</strong>
                                                                    <br>
                                                                    <strong>{{ $u->getEmployee->getposition->name }}</strong>
                                                                </div>
                                                            </div>
                                                            <hr class="my-4" />           
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="text-muted">Date Filed</div>
                                                                    <strong>{{ \Carbon\Carbon::parse($u->created_at)->format('M d,Y') }}</strong>
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                    <div class="text-muted">Undertime Date</div>
                                                                    <strong> {{ \Carbon\Carbon::parse($u->date)->format('M d,Y')}}</strong>
                                                                </div>
                                                            </div>
                                                                <br>
                                                            <div class="row mb-12">
                                                                <div class="col-md-6">
                                                                    <div class="text-muted">Reason</div>
                                                                   
                                                                    <p>
                                                                        <strong>{{ $u->reason }}</strong>
                                                                    </p>
                                                                </div>
                                                              
                                                               
                                                            </div>

                                                            <hr class="my-4" />

                                                            <div class="row mb-4">
                                                                <div class="col-md-6">
                                                                    <div class="text-muted">Status</div>
                                                                    <strong id="statusmodal{{$u->id}}">
                                                                    <span class="{{ $u->getRequestStatus->class }}">{{ $u->getRequestStatus->name }} </span>
                                                                    </strong>
                                                                </div>
                                                                <div class="col-md-6 text-md-right">
                                                                  @if($u->approved_by != null)  
                                                                    @if($u->approved_by == Auth::user()->id)
                                                                        <div class="text-muted">
                                                                            @if($u->status == 1)
                                                                                Approved By
                                                                            @elseif($u->status == 2)
                                                                                Disapproved By
                                                                            @endif
                                                                            </div>
                                                                            <strong>
                                                                                You
                                                                            </strong>
                                                                        </div> 
                                                                    @else
                                                                         <div class="text-muted">
                                                                         @if($u->status == 1)
                                                                                Approved By
                                                                            @elseif($u->status == 2)
                                                                                Disapproved By
                                                                            @endif
                                                                         </div>
                                                                            <strong>
                                                                                {{ $u->getApproval->name }}
                                                                            </strong>
                                                                        </div>    
                                                                    @endif
                                                                @else
                                                                        <div class="text-muted"></div>
                                                                            <strong>
                                                                                
                                                                            </strong>
                                                                        </div> 
                                                                @endif
                                                            </div>
                                                            @if(Auth::user()->user_type !=3 && ($u->status == 4))
                                                            <br>
                                                            <div class="row mb-4" id="button-approval{{$u->id}}">
                                                                <div class="col-md-2"></div>
                                                                <div class="col-md-4">
                                                                    <input type="hidden" name="undertime-id" id="undertime-id{{$u->id}}" value="{{ $u->id }}">
                                                                    <button class="btn btn-primary align-middle" value="1" onClick="update(this.value,{{$u->id}});"><i data-feather="thumbs-up"></i> Approved</button>
                                                                </div>
                                                                <div class="col-md-4 text-md-right">
                                                                    <button class="btn btn-danger align-middle"  value="2" onClick="update(this.value,{{$u->id}});"><i data-feather="thumbs-down"></i> Disapproved</button>
                                                                </div>  
                                                                <div class="col-md-2"></div>
                                                            </div>
                                                            @endif
                                                        </div>
                                                    
                                                </div>
                                             </div>
                                            <!-- <div class="modal-footer">                                               
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>                                                
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                </div>
             </div>
        </div>


    </div>
@endsection

@section('custom_js')
<script src="{{ asset('js/sweetalert.min.js') }}" ></script>
    <script>
		$(function() {
			// Datatables Responsive
			$("#table-undertime").DataTable({
				responsive: true
			});

            $('#datetimepicker-date').datetimepicker({
				format: 'L',
                minDate : new Date()
            });
            
                       
		});

       function cancel(id){
           var url = '/undertime/cancel-request';
             $.ajax({
                    type:"POST",
                    url:url,
                    data:{
                    id:id,                            
                    _token: "{{ csrf_token() }}",       
                    },
                    success:function(result){
                        toastr["info"](result);
                        $("#status"+id).html("<span id='statustext"+id+"'>Cancelled</span>");
                        $("#statustext"+id).addClass("badge badge-danger");

                        $("#statusmodal"+id).html("<span id='statusmodaltext"+id+"'>Cancelled</span>");
                        $("#statusmodaltext"+id).addClass("badge badge-danger");
                        
                    },
                    error:function(error){
                        console.log(error);
                        toastr["error"](error);
                    }
                    
            });

         
            
          }  

          function update(status_id,undertime_id){
            var url = '/undertime/update-request';
            
            var message = "";
            var word = "";

            if(parseInt(status_id) == 1){
                message = "Yes, approve it!";
                word = "approved";
            }else{
           
                message = "Yes, disapproved it!";
                word = "disapproved";
            }
            swal({
                        title: "",
                        text: "Are you sure want to "+word+" this undertime request?",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: message,
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: true,
                        closeOnCancel: true },
                    function (isConfirm) {
                        if(isConfirm){
                             $.ajax({
                                    type:"POST",
                                    url:url,
                                    data:{
                                    id:undertime_id,
                                    status:status_id,                            
                                    _token: "{{ csrf_token() }}",       
                                    },
                                    success:function(result){
                                        toastr["info"](result);
                                        if(parseInt(status_id) == 1){
                                            $("#status"+undertime_id).html("<span id='statustext"+undertime_id+"'>Approved</span>");
                                            $("#statustext"+undertime_id).addClass("badge badge-primary");

                                            $("#statusmodal"+undertime_id).html("<span id='statusmodaltext"+undertime_id+"'>Approved</span>");
                                            $("#statusmodaltext"+undertime_id).addClass("badge badge-primary");    
                                            $("#undertime"+undertime_id).modal('hide');
                                            $("#button-approval"+undertime_id).hide();
                                        }else{   
                                            $("#undertime"+undertime_id).modal('hide');
                                            $("#button-approval"+undertime_id).hide();
                                            $("#status"+undertime_id).html("<span id='statustext"+undertime_id+"'>Disapproved</span>");
                                            $("#statustext"+undertime_id).addClass("badge badge-danger");

                                            $("#statusmodal"+undertime_id).html("<span id='statusmodaltext"+undertime_id+"'>Disapproved</span>");
                                            $("#statusmodaltext"+undertime_id).addClass("badge badge-danger");    
                                        }                    
                                    },
                                    error:function(error){
                                        console.log(error);
                                        toastr["error"](error);
                                    }
                                    
                            });
                        }
                    });
        
       }
	</script>
@endsection
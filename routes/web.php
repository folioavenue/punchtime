<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landingpage');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/subscription','SubscriptionController');
Route::post('/subscription/delete-session','SubscriptionController@deleteSession');
Route::post('/subscription-payment','Auth\RegisterController@pay');
Route::get('/subscription-payment-status','Auth\RegisterController@paymentStatus');
Route::resource('/department','DepartmentController');
Route::resource('/position','PositionController');
Route::resource('/status','StatusController');
Route::resource('/dtr','DtrController');

Route::resource('/leave','LeaveController');
Route::post('/leave/cancel-request','LeaveController@cancelRequest');
Route::post('/leave/update-request','LeaveController@updateRequest');


Route::resource('/overtime','OvertimeController');
Route::post('/overtime/cancel-request','OvertimeController@cancelRequest');
Route::post('/overtime/update-request','OvertimeController@updateRequest');

Route::resource('/undertime','UndertimeController');
Route::post('/undertime/cancel-request','UndertimeController@cancelRequest');
Route::post('/undertime/update-request','UndertimeController@updateRequest');

Route::resource('/shift','ShiftController');
Route::post('/shift/break-schedule','ShiftController@storebreakschedule');
Route::resource('/employee','EmployeeController');

